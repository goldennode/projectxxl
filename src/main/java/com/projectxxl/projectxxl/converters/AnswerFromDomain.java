package com.projectxxl.projectxxl.converters;

import java.util.Arrays;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.AnswerDTO;
import com.projectxxl.projectxxl.domain.model.AnswerEntity;

@Component
public class AnswerFromDomain implements Converter<AnswerEntity, AnswerDTO> {
	@Override
	public AnswerDTO convert(AnswerEntity source) {
		if (source == null) {
			return null;
		}
		return new AnswerDTO.Builder().withAnswer(source.getAnswer()).withId(source.getId()).withQuestion_id(source.getQuestion_id()).withUser_id(source.getUser_id())
				.withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}

	public static AnswerDTO[] convertEntities(AnswerEntity[] entities, ConversionService conversionService) {
		if (entities == null) {
			return null;
		}
		return Arrays.stream(entities).map(entity -> conversionService.convert(entity, AnswerDTO.class)).toArray(AnswerDTO[]::new);
	}
}