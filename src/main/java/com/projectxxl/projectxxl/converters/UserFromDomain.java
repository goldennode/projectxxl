package com.projectxxl.projectxxl.converters;

import java.util.Arrays;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.UserDTO;
import com.projectxxl.projectxxl.domain.model.UserEntity;

@Component
public class UserFromDomain implements Converter<UserEntity, UserDTO> {
	@Override
	public UserDTO convert(UserEntity source) {
		if (source == null) {
			return null;
		}
		return new UserDTO.Builder().withAccess_key(source.getAccess_key()).withFull_name(source.getFull_name()).withId(source.getId()).withPassword(source.getPassword())
				.withSecret_key(source.getSecret_key()).withUser_name(source.getUser_name()).withUser_unique_id(source.getUser_unique_id()).withCreatedAt(source.getCreatedAt())
				.withModifiedAt(source.getModifiedAt()).build();
	}

	public static UserDTO[] convertEntities(UserEntity[] entities, ConversionService conversionService) {
		if (entities == null) {
			return null;
		}
		return Arrays.stream(entities).map(entity -> conversionService.convert(entity, UserDTO.class)).toArray(UserDTO[]::new);
	}
}