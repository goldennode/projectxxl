package com.projectxxl.projectxxl.converters;

import java.util.Arrays;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.QuestionDTO;
import com.projectxxl.projectxxl.domain.model.QuestionEntity;

@Component
public class QuestionFromDomain implements Converter<QuestionEntity, QuestionDTO> {
	@Override
	public QuestionDTO convert(QuestionEntity source) {
		if (source == null) {
			return null;
		}
		return new QuestionDTO.Builder().withId(source.getId()).withQuestion(source.getQuestion()).withUser_id(source.getUser_id()).withCreatedAt(source.getCreatedAt())
				.withModifiedAt(source.getModifiedAt()).build();
	}

	public static QuestionDTO[] convertEntities(QuestionEntity[] entities, ConversionService conversionService) {
		if (entities == null) {
			return null;
		}
		return Arrays.stream(entities).map(entity -> conversionService.convert(entity, QuestionDTO.class)).toArray(QuestionDTO[]::new);
	}
}