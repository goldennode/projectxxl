package com.projectxxl.projectxxl.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.RatingDTO;
import com.projectxxl.projectxxl.domain.model.RatingEntity;

@Component
public class RatingToDomain implements Converter<RatingDTO, RatingEntity> {
	@Override
	public RatingEntity convert(RatingDTO source) {
		if (source == null) {
			return null;
		}
		return new RatingEntity.Builder().withQuestion_id(source.getQuestion_id()).withAnswer_id(source.getAnswer_id()).withId(source.getId()).withMode(source.getMode())
				.withRating_communication(source.getRating_communication()).withRating_knowledge(source.getRating_knowledge()).withRating_overall(source.getRating_overall())
				.withRating_response(source.getRating_response()).withRating_text(source.getRating_text()).withUser_id_for(source.getUser_id_for())
				.withUser_id_from(source.getUser_id_from()).withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}
}