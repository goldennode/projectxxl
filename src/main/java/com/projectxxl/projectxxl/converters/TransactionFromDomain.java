package com.projectxxl.projectxxl.converters;

import java.util.Arrays;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.TransactionDTO;
import com.projectxxl.projectxxl.domain.model.TransactionEntity;

@Component
public class TransactionFromDomain implements Converter<TransactionEntity, TransactionDTO> {
	@Override
	public TransactionDTO convert(TransactionEntity source) {
		if (source == null) {
			return null;
		}
		return new TransactionDTO.Builder().withAmount(source.getAmount()).withAnswer_id(source.getAnswer_id()).withId(source.getId()).withQuestion_id(source.getQuestion_id())
				.withStatus(source.getStatus()).withType(source.getType()).withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}

	public static TransactionDTO[] convertEntities(TransactionEntity[] entities, ConversionService conversionService) {
		if (entities == null) {
			return null;
		}
		return Arrays.stream(entities).map(entity -> conversionService.convert(entity, TransactionDTO.class)).toArray(TransactionDTO[]::new);
	}
}