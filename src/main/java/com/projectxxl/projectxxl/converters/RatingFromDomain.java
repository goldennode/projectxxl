package com.projectxxl.projectxxl.converters;

import java.util.Arrays;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.RatingDTO;
import com.projectxxl.projectxxl.domain.model.RatingEntity;

@Component
public class RatingFromDomain implements Converter<RatingEntity, RatingDTO> {
	@Override
	public RatingDTO convert(RatingEntity source) {
		if (source == null) {
			return null;
		}
		return new RatingDTO.Builder().withQuestion_id(source.getQuestion_id()).withAnswer_id(source.getAnswer_id()).withId(source.getId()).withMode(source.getMode())
				.withRating_communication(source.getRating_communication()).withRating_knowledge(source.getRating_knowledge()).withRating_overall(source.getRating_overall())
				.withRating_response(source.getRating_response()).withRating_text(source.getRating_text()).withUser_id_for(source.getUser_id_for())
				.withUser_id_from(source.getUser_id_from()).withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}

	public static RatingDTO[] convertEntities(RatingEntity[] entities, ConversionService conversionService) {
		if (entities == null) {
			return null;
		}
		return Arrays.stream(entities).map(entity -> conversionService.convert(entity, RatingDTO.class)).toArray(RatingDTO[]::new);
	}
}