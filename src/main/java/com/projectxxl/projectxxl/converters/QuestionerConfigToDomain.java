package com.projectxxl.projectxxl.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.QuestionerConfigDTO;
import com.projectxxl.projectxxl.domain.model.QuestionerConfigEntity;

@Component
public class QuestionerConfigToDomain implements Converter<QuestionerConfigDTO, QuestionerConfigEntity> {
	@Override
	public QuestionerConfigEntity convert(QuestionerConfigDTO source) {
		if (source == null) {
			return null;
		}
		return new QuestionerConfigEntity.Builder().withAnswerer_norating_acceptable(source.getAnswerer_norating_acceptable())
				.withAnswerer_rating_min(source.getAnswerer_rating_min()).withCost_max(source.getCost_max()).withCost_min(source.getCost_min()).withId(source.getId())
				.withUser_id(source.getUser_id()).withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}
}