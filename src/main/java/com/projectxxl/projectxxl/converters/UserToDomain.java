package com.projectxxl.projectxxl.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.UserDTO;
import com.projectxxl.projectxxl.domain.model.UserEntity;

@Component
public class UserToDomain implements Converter<UserDTO, UserEntity> {
	@Override
	public UserEntity convert(UserDTO source) {
		if (source == null) {
			return null;
		}
		return new UserEntity.Builder().withAccess_key(source.getAccess_key()).withFull_name(source.getFull_name()).withId(source.getId()).withPassword(source.getPassword())
				.withSecret_key(source.getSecret_key()).withUser_name(source.getUser_name()).withUser_unique_id(source.getUser_unique_id()).withCreatedAt(source.getCreatedAt())
				.withModifiedAt(source.getModifiedAt()).build();
	}
}