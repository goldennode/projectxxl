package com.projectxxl.projectxxl.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.QuestionDTO;
import com.projectxxl.projectxxl.domain.model.QuestionEntity;

@Component
public class QuestionToDomain implements Converter<QuestionDTO, QuestionEntity> {
	@Override
	public QuestionEntity convert(QuestionDTO source) {
		if (source == null) {
			return null;
		}
		return new QuestionEntity.Builder().withId(source.getId()).withQuestion(source.getQuestion()).withUser_id(source.getUser_id()).withCreatedAt(source.getCreatedAt())
				.withModifiedAt(source.getModifiedAt()).build();
	}
}