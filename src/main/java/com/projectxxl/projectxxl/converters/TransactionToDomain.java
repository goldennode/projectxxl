package com.projectxxl.projectxxl.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.TransactionDTO;
import com.projectxxl.projectxxl.domain.model.TransactionEntity;

@Component
public class TransactionToDomain implements Converter<TransactionDTO, TransactionEntity> {
	@Override
	public TransactionEntity convert(TransactionDTO source) {
		if (source == null) {
			return null;
		}
		return new TransactionEntity.Builder().withAmount(source.getAmount()).withAnswer_id(source.getAnswer_id()).withId(source.getId()).withQuestion_id(source.getQuestion_id())
				.withStatus(source.getStatus()).withType(source.getType()).withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}
}