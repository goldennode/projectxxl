package com.projectxxl.projectxxl.converters;

import java.util.Arrays;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.AnswererConfigDTO;
import com.projectxxl.projectxxl.domain.model.AnswererConfigEntity;

@Component
public class AnswererConfigFromDomain implements Converter<AnswererConfigEntity, AnswererConfigDTO> {
	@Override
	public AnswererConfigDTO convert(AnswererConfigEntity source) {
		if (source == null) {
			return null;
		}
		return new AnswererConfigDTO.Builder().withCost_max(source.getCost_max()).withCost_min(source.getCost_min()).withId(source.getId()).withTag(source.getTag())
				.withUser_id(source.getUser_id()).withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}

	public static AnswererConfigDTO[] convertEntities(AnswererConfigEntity[] entities, ConversionService conversionService) {
		if (entities == null) {
			return null;
		}
		return Arrays.stream(entities).map(entity -> conversionService.convert(entity, AnswererConfigDTO.class)).toArray(AnswererConfigDTO[]::new);
	}
}