package com.projectxxl.projectxxl.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.AnswerDTO;
import com.projectxxl.projectxxl.domain.model.AnswerEntity;

@Component
public class AnswerToDomain implements Converter<AnswerDTO, AnswerEntity> {
	@Override
	public AnswerEntity convert(AnswerDTO source) {
		if (source == null) {
			return null;
		}
		return new AnswerEntity.Builder().withAnswer(source.getAnswer()).withId(source.getId()).withQuestion_id(source.getQuestion_id()).withUser_id(source.getUser_id())
				.withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}
}