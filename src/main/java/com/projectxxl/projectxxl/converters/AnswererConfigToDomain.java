package com.projectxxl.projectxxl.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.projectxxl.projectxxl.controllers.dto.AnswererConfigDTO;
import com.projectxxl.projectxxl.domain.model.AnswererConfigEntity;

@Component
public class AnswererConfigToDomain implements Converter<AnswererConfigDTO, AnswererConfigEntity> {
	@Override
	public AnswererConfigEntity convert(AnswererConfigDTO source) {
		if (source == null) {
			return null;
		}
		return new AnswererConfigEntity.Builder().withCost_max(source.getCost_max()).withCost_min(source.getCost_min()).withId(source.getId()).withTag(source.getTag())
				.withUser_id(source.getUser_id()).withCreatedAt(source.getCreatedAt()).withModifiedAt(source.getModifiedAt()).build();
	}
}