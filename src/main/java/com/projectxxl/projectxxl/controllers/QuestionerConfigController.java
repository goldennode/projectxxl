package com.projectxxl.projectxxl.controllers;

import static com.google.common.base.Preconditions.checkArgument;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_CREATED;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NO_CONTENT;
import static javax.servlet.http.HttpServletResponse.SC_OK;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.util.UriComponentsBuilder;

import com.projectxxl.projectxxl.controllers.dto.QuestionerConfigDTO;
import com.projectxxl.projectxxl.controllers.dto.exception.ApiError;
import com.projectxxl.projectxxl.controllers.dto.exception.ErrorCode;
import com.projectxxl.projectxxl.controllers.dto.exception.ProjectXXLRestException;
import com.projectxxl.projectxxl.converters.QuestionerConfigFromDomain;
import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.QuestionerConfigEntity;
import com.projectxxl.projectxxl.service.QuestionerConfigService;
import com.projectxxl.projectxxl.utils.LinkUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller for basic entity operations
 *
 */
@RestController
@RequestMapping(value = { "/questionerconfig" })
@CrossOrigin(origins = "*")
public class QuestionerConfigController {
	private final QuestionerConfigService entityService;
	private final ConversionService conversionService;
	private final String maxPageSize;
	Logger logger = LoggerFactory.getLogger(QuestionerConfigController.class);

	@Autowired
	public QuestionerConfigController(QuestionerConfigService entityService, ConversionService conversionService, @Value("${pagination.maxPageSize}") String maxPageSize) {
		this.entityService = entityService;
		this.conversionService = conversionService;
		this.maxPageSize = maxPageSize;
	}

	@RequestMapping(value = { "/{id}" }, method = { RequestMethod.GET })
	@ApiOperation(value = "Gets a single entity created", notes = "Provide id in the path")
	@ApiResponses({ @ApiResponse(code = SC_OK, response = QuestionerConfigDTO.class, message = "entity returned successfully"),
			@ApiResponse(code = SC_BAD_REQUEST, response = ApiError.class, message = "entity not found"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error occurred while processing your request") })
	public ResponseEntity<QuestionerConfigDTO> getEntity(@PathVariable("id") String id, HttpServletResponse response) throws ProjectXXLRestException {
		try {
			QuestionerConfigEntity entity = entityService.getEntity(id);
			return new ResponseEntity<QuestionerConfigDTO>(conversionService.convert(entity, QuestionerConfigDTO.class), HttpStatus.OK);
		} catch (EntityNotFoundException e) {
			throw new ProjectXXLRestException(ErrorCode.ENTITY_NOT_FOUND);
		}
	}

	@RequestMapping(method = { RequestMethod.POST })
	@ApiOperation(value = "Creates a new entity", notes = "Provide DTO object in the request")
	@ApiResponses({ @ApiResponse(code = SC_CREATED, response = QuestionerConfigDTO.class, message = "Created entity"),
			@ApiResponse(code = SC_BAD_REQUEST, response = ApiError.class, message = "Entity verification fails"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error occurred while processing your request") })
	public ResponseEntity<QuestionerConfigDTO> createEntity(@RequestBody QuestionerConfigDTO dto, HttpServletResponse response) throws ProjectXXLRestException {
		try {
			QuestionerConfigEntity entity = entityService.createEntity(conversionService.convert(dto, QuestionerConfigEntity.class));
			return new ResponseEntity<QuestionerConfigDTO>(conversionService.convert(entity, QuestionerConfigDTO.class), HttpStatus.CREATED);
		} catch (EntityAlreadyExistsException e) {
			throw new ProjectXXLRestException(ErrorCode.ENTITY_ALREADY_EXISTS);
		}
	}

	@RequestMapping(value = { "/{id}" }, method = { RequestMethod.PUT })
	@ApiOperation(value = "Updates an existing entity", notes = "Provide DTO object in the request and id in the path")
	@ApiResponses({ @ApiResponse(code = SC_OK, response = QuestionerConfigDTO.class, message = "Updated entity"),
			@ApiResponse(code = SC_BAD_REQUEST, response = ApiError.class, message = "Entity not found or entity verification fails"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error occurred while processing your request") })
	public ResponseEntity<QuestionerConfigDTO> updateEntity(@PathVariable("id") String id, @RequestBody QuestionerConfigDTO dto, HttpServletResponse response)
			throws ProjectXXLRestException {
		try {
			return new ResponseEntity<QuestionerConfigDTO>(conversionService.convert(
					entityService.updateEntity(
							entityService.getEntity(id).toBuilder().withUser_id(dto.getUser_id()).withAnswerer_norating_acceptable(dto.getAnswerer_norating_acceptable())
									.withAnswerer_rating_min(dto.getAnswerer_rating_min()).withCost_max(dto.getCost_max()).withCost_min(dto.getCost_min()).build()),
					QuestionerConfigDTO.class), HttpStatus.OK);
		} catch (EntityNotFoundException e) {
			throw new ProjectXXLRestException(ErrorCode.ENTITY_NOT_FOUND);
		}
	}

	@RequestMapping(value = { "/{id}" }, method = { RequestMethod.DELETE })
	@ApiOperation(value = "Deletes an existing entity", notes = "Provide id in the path")
	@ApiResponses({ @ApiResponse(code = SC_NO_CONTENT, response = QuestionerConfigDTO.class, message = "Deleted entity"),
			@ApiResponse(code = SC_BAD_REQUEST, response = ApiError.class, message = "Entity not found"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error occurred while processing your request") })
	public ResponseEntity<QuestionerConfigDTO> deleteEntity(@PathVariable("id") String id, HttpServletResponse response) throws ProjectXXLRestException {
		try {
			entityService.deleteEntity(id);
			return new ResponseEntity<QuestionerConfigDTO>(HttpStatus.NO_CONTENT);
		} catch (EntityNotFoundException e) {
			throw new ProjectXXLRestException(ErrorCode.ENTITY_NOT_FOUND);
		}
	}

	@RequestMapping(method = { RequestMethod.GET })
	@ApiOperation(value = "Gets all the entities created sorted by natural ordering", notes = "No parameter required")
	@ApiResponses({ @ApiResponse(code = SC_OK, response = QuestionerConfigDTO.class, message = "Entities returned successfully"),
			@ApiResponse(code = SC_BAD_REQUEST, response = ApiError.class, message = "Page not found"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error occurred while processing your request") })
	public ResponseEntity<QuestionerConfigDTO[]> getAllEntitys(@RequestParam(value = "page", defaultValue = "0") Integer page,
			@RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, UriComponentsBuilder uriBuilder, HttpServletResponse response, HttpServletRequest request)
			throws ProjectXXLRestException {
		try {
			logger.info("request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE)=" + request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE));
			checkArgument(pageSize <= Integer.parseInt(maxPageSize) && pageSize > 0, "Page size should be between 1 and " + maxPageSize);
			PageData<QuestionerConfigEntity> pagedData = null;
			pagedData = entityService.getAllEntities(page, pageSize);
			LinkUtil.addLinkHeaderOnPagedResourceRetrieval(uriBuilder, response, (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE), pagedData.getPage(),
					pagedData.getMaxPages(), pagedData.getPageSize());
			return new ResponseEntity<QuestionerConfigDTO[]>(QuestionerConfigFromDomain.convertEntities(pagedData.getEntityArray(), conversionService), HttpStatus.OK);
		} catch (PageNotFoundException e) {
			throw new ProjectXXLRestException(ErrorCode.PAGE_NOT_FOUND);
		}
	}
}