package com.projectxxl.projectxxl.controllers;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_OK;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.projectxxl.projectxxl.controllers.dto.AnswerDTO;
import com.projectxxl.projectxxl.controllers.dto.exception.ApiError;
import com.projectxxl.projectxxl.controllers.dto.exception.ErrorCode;
import com.projectxxl.projectxxl.controllers.dto.exception.ProjectXXLRestException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Controller for basic entity operations
 *
 */
@RestController
@RequestMapping(value = { "/version" })
@CrossOrigin(origins = "*")
public class VersionController {
	private final String version;
	Logger logger = LoggerFactory.getLogger(VersionController.class);

	@Autowired
	public VersionController(@Value("${app.version}") String version) {
		this.version = version;
	}

	@RequestMapping(method = { RequestMethod.GET })
	@ApiOperation(value = "Version info", notes = "No parameter required")
	@ApiResponses({ @ApiResponse(code = SC_OK, response = AnswerDTO.class, message = "Successful"),
			@ApiResponse(code = SC_BAD_REQUEST, response = ApiError.class, message = "Page not found"),
			@ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = "Internal server error occurred while processing your request") })
	public ResponseEntity<String> getVersion() throws ProjectXXLRestException {
		try {
			Enumeration<NetworkInterface> iterNetwork;
			Enumeration<InetAddress> iterAddress;
			NetworkInterface network;
			InetAddress address;
			iterNetwork = NetworkInterface.getNetworkInterfaces();
			StringBuilder addr = new StringBuilder();
			while (iterNetwork.hasMoreElements()) {
				network = iterNetwork.nextElement();
				if (!network.isUp())
					continue;
				if (network.isLoopback())
					continue;
				iterAddress = network.getInetAddresses();
				while (iterAddress.hasMoreElements()) {
					address = iterAddress.nextElement();
					if (address.isMulticastAddress())
						continue;
					if (address.isLoopbackAddress())
						continue;
					if (address.isAnyLocalAddress())
						continue;
					addr.append(network.getDisplayName() + ": " + address.getHostAddress() + "\n");
				}
			}
			String output = "Version:" + version + "\n" + addr.toString();
			return new ResponseEntity<String>(output, HttpStatus.OK);
		} catch (SocketException e) {
			throw new ProjectXXLRestException(ErrorCode.GENERAL_ERROR);
		}
	}
}