package com.projectxxl.projectxxl.controllers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.projectxxl.projectxxl.utils.RandomUtils;

public class QuestionerConfigDTO extends BaseDTO {
	@JsonProperty
	private final String user_id;
	@JsonProperty
	private final Double cost_min;
	@JsonProperty
	private final Double cost_max;
	@JsonProperty
	private final Double answerer_rating_min;
	@JsonProperty
	private final Integer answerer_norating_acceptable;

	public QuestionerConfigDTO() {
		super();
		this.user_id = null;
		this.cost_min = null;
		this.cost_max = null;
		this.answerer_rating_min = null;
		this.answerer_norating_acceptable = null;
	}

	public QuestionerConfigDTO(Builder builder) {
		super(builder);
		this.user_id = builder.user_id;
		this.cost_min = builder.cost_min;
		this.cost_max = builder.cost_max;
		this.answerer_rating_min = builder.answerer_rating_min;
		this.answerer_norating_acceptable = builder.answerer_norating_acceptable;
	}

	public String getId() {
		return id;
	}

	public String getUser_id() {
		return user_id;
	}

	public double getCost_min() {
		return cost_min;
	}

	public double getCost_max() {
		return cost_max;
	}

	public double getAnswerer_rating_min() {
		return answerer_rating_min;
	}

	public int getAnswerer_norating_acceptable() {
		return answerer_norating_acceptable;
	}

	public Builder toBuilder() {
		return new QuestionerConfigDTO.Builder(this);
	}

	public static QuestionerConfigDTO createRandom() {
		return new QuestionerConfigDTO.Builder().withAnswerer_rating_min(RandomUtils.randomDouble(5)).withAnswerer_norating_acceptable(RandomUtils.randomInt(1))
				.withCost_min(RandomUtils.randomDouble(1, 5)).withCost_max(RandomUtils.randomDouble(10, 100)).withUser_id(RandomUtils.randomString("user_id")).build();
	}

	public static class Builder extends BaseDTOBuilder {
		private String user_id;
		private Double cost_min;
		private Double cost_max;
		private Double answerer_rating_min;
		private Integer answerer_norating_acceptable;

		public Builder(QuestionerConfigDTO entity) {
			super(entity);
			this.user_id = entity.user_id;
			this.cost_min = entity.cost_min;
			this.cost_max = entity.cost_max;
			this.answerer_rating_min = entity.answerer_rating_min;
			this.answerer_norating_acceptable = entity.answerer_norating_acceptable;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public String getUser_id() {
			return user_id;
		}

		public Builder withUser_id(String user_id) {
			this.user_id = user_id;
			return this;
		}

		public Double getCost_min() {
			return cost_min;
		}

		public Builder withCost_min(Double cost_min) {
			this.cost_min = cost_min;
			return this;
		}

		public Double getCost_max() {
			return cost_max;
		}

		public Builder withCost_max(Double cost_max) {
			this.cost_max = cost_max;
			return this;
		}

		public Double getAnswerer_rating_min() {
			return answerer_rating_min;
		}

		public Builder withAnswerer_rating_min(Double answerer_rating_min) {
			this.answerer_rating_min = answerer_rating_min;
			return this;
		}

		public Integer getAnswerer_norating_acceptable() {
			return answerer_norating_acceptable;
		}

		public Builder withAnswerer_norating_acceptable(Integer answerer_norating_acceptable) {
			this.answerer_norating_acceptable = answerer_norating_acceptable;
			return this;
		}

		public QuestionerConfigDTO build() {
			return new QuestionerConfigDTO(this);
		}
	}
}
