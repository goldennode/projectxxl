package com.projectxxl.projectxxl.controllers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.projectxxl.projectxxl.utils.RandomUtils;

public class RatingDTO extends BaseDTO {
	@JsonProperty
	private final String user_id_from;
	@JsonProperty
	private final String user_id_for;
	@JsonProperty
	private final String question_id;
	@JsonProperty
	private final String answer_id;
	@JsonProperty
	private final Integer mode;
	@JsonProperty
	private final Integer rating_knowledge;
	@JsonProperty
	private final Integer rating_communication;
	@JsonProperty
	private final Integer rating_overall;
	@JsonProperty
	private final String rating_text;
	@JsonProperty
	private final String rating_response;

	public RatingDTO() {
		super();
		this.user_id_from = null;
		this.user_id_for = null;
		this.question_id = null;
		this.answer_id = null;
		this.mode = null;
		this.rating_knowledge = null;
		this.rating_communication = null;
		this.rating_overall = null;
		this.rating_text = null;
		this.rating_response = null;
	}

	public RatingDTO(Builder builder) {
		super(builder);
		this.user_id_from = builder.user_id_from;
		this.user_id_for = builder.user_id_for;
		this.question_id = builder.question_id;
		this.answer_id = builder.answer_id;
		;
		this.mode = builder.mode;
		this.rating_knowledge = builder.rating_knowledge;
		this.rating_communication = builder.rating_communication;
		this.rating_overall = builder.rating_overall;
		this.rating_text = builder.rating_text;
		this.rating_response = builder.rating_response;
	}

	public String getId() {
		return id;
	}

	public String getUser_id_from() {
		return user_id_from;
	}

	public String getUser_id_for() {
		return user_id_for;
	}

	public String getQuestion_id() {
		return question_id;
	}

	public String getAnswer_id() {
		return answer_id;
	}

	public Integer getMode() {
		return mode;
	}

	public Integer getRating_knowledge() {
		return rating_knowledge;
	}

	public Integer getRating_communication() {
		return rating_communication;
	}

	public Integer getRating_overall() {
		return rating_overall;
	}

	public String getRating_text() {
		return rating_text;
	}

	public String getRating_response() {
		return rating_response;
	}

	public Builder toBuilder() {
		return new RatingDTO.Builder(this);
	}

	public static RatingDTO createRandom() {
		return new RatingDTO.Builder().withQuestion_id(RandomUtils.randomString("question_id")).withAnswer_id(RandomUtils.randomString("answer_id"))
				.withMode(RandomUtils.randomInt(1)).withRating_communication(RandomUtils.randomInt(1, 5)).withRating_knowledge(RandomUtils.randomInt(1, 5))
				.withRating_overall(RandomUtils.randomInt(1, 5)).withRating_response(RandomUtils.randomString("rating_response"))
				.withRating_text(RandomUtils.randomString("rating_text")).withUser_id_for(RandomUtils.randomString("user_id_for"))
				.withUser_id_from(RandomUtils.randomString("user_id_from")).build();
	}

	public static class Builder extends BaseDTOBuilder {
		private String user_id_from;
		private String user_id_for;
		private String question_id;
		private String answer_id;
		private Integer mode;
		private Integer rating_knowledge;
		private Integer rating_communication;
		private Integer rating_overall;
		private String rating_text;
		private String rating_response;

		public Builder(RatingDTO entity) {
			super(entity);
			this.user_id_from = entity.user_id_from;
			this.user_id_for = entity.user_id_for;
			this.question_id = entity.question_id;
			this.answer_id = entity.answer_id;
			this.mode = entity.mode;
			this.rating_knowledge = entity.rating_knowledge;
			this.rating_communication = entity.rating_communication;
			this.rating_overall = entity.rating_overall;
			this.rating_text = entity.rating_text;
			this.rating_response = entity.rating_response;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public String getUser_id_from() {
			return user_id_from;
		}

		public Builder withUser_id_from(String user_id_from) {
			this.user_id_from = user_id_from;
			return this;
		}

		public String getUserid_for() {
			return user_id_for;
		}

		public Builder withUser_id_for(String user_id_for) {
			this.user_id_for = user_id_for;
			return this;
		}

		public String getQuestion_id() {
			return question_id;
		}

		public Builder withQuestion_id(String question_id) {
			this.question_id = question_id;
			return this;
		}

		public String getAnswer_id() {
			return answer_id;
		}

		public Builder withAnswer_id(String answer_id) {
			this.answer_id = answer_id;
			return this;
		}

		public Integer getMode() {
			return mode;
		}

		public Builder withMode(Integer mode) {
			this.mode = mode;
			return this;
		}

		public Integer getRating_knowledge() {
			return rating_knowledge;
		}

		public Builder withRating_knowledge(Integer rating_knowledge) {
			this.rating_knowledge = rating_knowledge;
			return this;
		}

		public Integer getRating_communication() {
			return rating_communication;
		}

		public Builder withRating_communication(Integer rating_communication) {
			this.rating_communication = rating_communication;
			return this;
		}

		public Integer getRating_overall() {
			return rating_overall;
		}

		public Builder withRating_overall(Integer rating_overall) {
			this.rating_overall = rating_overall;
			return this;
		}

		public String getRating_text() {
			return rating_text;
		}

		public Builder withRating_text(String rating_text) {
			this.rating_text = rating_text;
			return this;
		}

		public String getRating_response() {
			return rating_response;
		}

		public Builder withRating_response(String rating_response) {
			this.rating_response = rating_response;
			return this;
		}

		public RatingDTO build() {
			return new RatingDTO(this);
		}
	}
}
