package com.projectxxl.projectxxl.controllers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.projectxxl.projectxxl.utils.RandomUtils;

public class AnswerDTO extends BaseDTO {
	@JsonProperty
	private final String user_id;
	@JsonProperty
	private final String question_id;
	@JsonProperty
	private final String answer;

	public AnswerDTO() {
		super();
		this.user_id = null;
		this.question_id = null;
		this.answer = null;
	}

	public AnswerDTO(Builder builder) {
		super(builder);
		this.user_id = builder.user_id;
		this.question_id = builder.question_id;
		this.answer = builder.answer;
	}

	public String getId() {
		return id;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getQuestion_id() {
		return question_id;
	}

	public String getAnswer() {
		return answer;
	}

	public Builder toBuilder() {
		return new AnswerDTO.Builder(this);
	}

	public static AnswerDTO createRandom() {
		return new AnswerDTO.Builder().withQuestion_id(RandomUtils.randomString("question_id")).withAnswer(RandomUtils.randomString("answer"))
				.withUser_id(RandomUtils.randomString("user_id")).build();
	}

	public static class Builder extends BaseDTOBuilder {
		private String user_id;
		private String question_id;
		private String answer;

		public Builder(AnswerDTO entity) {
			super(entity);
			this.user_id = entity.user_id;
			this.question_id = entity.question_id;
			this.answer = entity.answer;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public String getUser_id() {
			return user_id;
		}

		public String getQuestion_id() {
			return question_id;
		}

		public String getAnswer() {
			return answer;
		}

		public Builder withUser_id(String user_id) {
			this.user_id = user_id;
			return this;
		}

		public Builder withQuestion_id(String question_id) {
			this.question_id = question_id;
			return this;
		}

		public Builder withAnswer(String answer) {
			this.answer = answer;
			return this;
		}

		public AnswerDTO build() {
			return new AnswerDTO(this);
		}
	}
}
