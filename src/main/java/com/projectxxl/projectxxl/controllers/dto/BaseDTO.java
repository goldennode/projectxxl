package com.projectxxl.projectxxl.controllers.dto;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;

public abstract class BaseDTO {
	@ApiModelProperty(value = "Unique identifier for this entity.", accessMode = AccessMode.READ_ONLY)
	@JsonProperty(access = READ_ONLY)
	protected final String id;
	@ApiModelProperty(value = "Unix epoch time for last modification date.", accessMode = AccessMode.READ_ONLY)
	@JsonProperty(access = READ_ONLY)
	protected final Long createdAt;
	@ApiModelProperty(value = "Unix epoch time for last modification date.", accessMode = AccessMode.READ_ONLY)
	@JsonProperty(access = READ_ONLY)
	protected final Long modifiedAt;

	public BaseDTO(BaseDTOBuilder builder) {
		this.id = builder.id;
		this.createdAt = builder.createdAt;
		this.modifiedAt = builder.modifiedAt;
	}

	public BaseDTO() {
		this.id = null;
		this.createdAt = null;
		this.modifiedAt = null;
	}

	public String getId() {
		return id;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public Long getModifiedAt() {
		return modifiedAt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseDTO other = (BaseDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
