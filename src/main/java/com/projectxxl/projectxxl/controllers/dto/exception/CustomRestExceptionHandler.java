package com.projectxxl.projectxxl.controllers.dto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

/**
 * This class contains handler methods for RESTAPI exceptions
 *
 */
@ControllerAdvice(basePackages = "com.projectxxl.projectxxl.controllers")
public class CustomRestExceptionHandler {
	/**
	 * 
	 * Handler for {@link ProjectXXLRestException}
	 * 
	 * @param ex      {@link ProjectXXLRestException}
	 * @param request {@link WebRequest}
	 * @return {@link ResponseEntity}
	 */
	@ExceptionHandler({ ProjectXXLRestException.class })
	public ResponseEntity<Object> handleAll(ProjectXXLRestException ex, WebRequest request) {
		ApiError apiError = new ApiError(ex.getClaz(), ex.getDescription());
		return new ResponseEntity<Object>(apiError, HttpStatus.BAD_REQUEST);
	}

	/**
	 * 
	 * Handler for all exceptions including {@link RuntimeException}
	 * 
	 * @param ex      {@link Exception}
	 * @param request {@link WebRequest}
	 * @return {@link ResponseEntity}
	 */
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
		ApiError apiError = new ApiError(ex.getClass().getName(), ex.getMessage() != null ? ex.getMessage() : "");
		return new ResponseEntity<Object>(apiError, HttpStatus.BAD_REQUEST);
	}
}
