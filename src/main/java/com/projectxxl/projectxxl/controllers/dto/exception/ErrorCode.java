package com.projectxxl.projectxxl.controllers.dto.exception;

/**
 * Specific error types for the RESTAPI
 *
 */
public enum ErrorCode {
	GENERAL_ERROR, ENTITY_NOT_FOUND, ENTITY_ALREADY_EXISTS, PAGE_NOT_FOUND;
}
