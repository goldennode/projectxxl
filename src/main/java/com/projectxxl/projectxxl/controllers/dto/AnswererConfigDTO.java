package com.projectxxl.projectxxl.controllers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.projectxxl.projectxxl.utils.RandomUtils;

public class AnswererConfigDTO extends BaseDTO {
	@JsonProperty
	private final String user_id;
	@JsonProperty
	private final String tag;
	@JsonProperty
	private final Double cost_min;
	@JsonProperty
	private final Double cost_max;

	public AnswererConfigDTO() {
		super();
		this.user_id = null;
		this.tag = null;
		this.cost_min = null;
		this.cost_max = null;
	}

	public AnswererConfigDTO(Builder builder) {
		super(builder);
		this.user_id = builder.user_id;
		this.tag = builder.tag;
		this.cost_min = builder.cost_min;
		this.cost_max = builder.cost_max;
	}

	public String getId() {
		return id;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getTag() {
		return tag;
	}

	public double getCost_min() {
		return cost_min;
	}

	public double getCost_max() {
		return cost_max;
	}

	public Builder toBuilder() {
		return new AnswererConfigDTO.Builder(this);
	}

	public static AnswererConfigDTO createRandom() {
		return new AnswererConfigDTO.Builder().withTag(RandomUtils.randomString("tag")).withCost_min(RandomUtils.randomDouble(1, 5)).withCost_max(RandomUtils.randomDouble(10, 100))
				.withUser_id(RandomUtils.randomString("user_id")).build();
	}

	public static class Builder extends BaseDTOBuilder {
		private String user_id;
		private String tag;
		private Double cost_min;
		private Double cost_max;

		public Builder(AnswererConfigDTO entity) {
			super(entity);
			this.user_id = entity.user_id;
			this.tag = entity.tag;
			this.cost_min = entity.cost_min;
			this.cost_max = entity.cost_max;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public String getUser_id() {
			return user_id;
		}

		public Builder withUser_id(String user_id) {
			this.user_id = user_id;
			return this;
		}

		public String getTag() {
			return tag;
		}

		public Builder withTag(String tag) {
			this.tag = tag;
			return this;
		}

		public Double getCost_min() {
			return cost_min;
		}

		public Builder withCost_min(Double cost_min) {
			this.cost_min = cost_min;
			return this;
		}

		public Double getCost_max() {
			return cost_max;
		}

		public Builder withCost_max(Double cost_max) {
			this.cost_max = cost_max;
			return this;
		}

		public AnswererConfigDTO build() {
			return new AnswererConfigDTO(this);
		}
	}
}
