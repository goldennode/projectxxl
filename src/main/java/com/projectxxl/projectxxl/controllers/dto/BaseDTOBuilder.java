package com.projectxxl.projectxxl.controllers.dto;

public abstract class BaseDTOBuilder {
	String id;
	Long createdAt;
	Long modifiedAt;

	public BaseDTOBuilder() {
	}

	public BaseDTOBuilder(BaseDTO entity) {
		this.id = entity.id;
		this.createdAt = entity.createdAt;
		this.modifiedAt = entity.modifiedAt;
	}

	public String getId() {
		return id;
	}

	public BaseDTOBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public BaseDTOBuilder withCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	public Long getModifiedAt() {
		return modifiedAt;
	}

	public BaseDTOBuilder withModifiedAt(Long modifiedAt) {
		this.modifiedAt = modifiedAt;
		return this;
	}
}
