package com.projectxxl.projectxxl.controllers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.projectxxl.projectxxl.utils.RandomUtils;

public class QuestionDTO extends BaseDTO {
	@JsonProperty
	private final String user_id;
	@JsonProperty
	private final String question;

	public QuestionDTO() {
		super();
		this.user_id = null;
		this.question = null;
	}

	public QuestionDTO(Builder builder) {
		super(builder);
		this.user_id = builder.user_id;
		this.question = builder.question;
	}

	public String getId() {
		return id;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getQuestion() {
		return question;
	}

	public Builder toBuilder() {
		return new QuestionDTO.Builder(this);
	}

	public static QuestionDTO createRandom() {
		return new QuestionDTO.Builder().withQuestion(RandomUtils.randomString("question")).withUser_id(RandomUtils.randomString("user_id")).build();
	}

	public static class Builder extends BaseDTOBuilder {
		private String user_id;
		private String question;

		public Builder(QuestionDTO entity) {
			super(entity);
			this.user_id = entity.user_id;
			this.question = entity.question;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public String getUser_id() {
			return user_id;
		}

		public Builder withUser_id(String user_id) {
			this.user_id = user_id;
			return this;
		}

		public String getQuestion() {
			return question;
		}

		public Builder withQuestion(String question) {
			this.question = question;
			return this;
		}

		public QuestionDTO build() {
			return new QuestionDTO(this);
		}
	}
}
