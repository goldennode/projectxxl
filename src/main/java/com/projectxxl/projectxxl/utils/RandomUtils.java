package com.projectxxl.projectxxl.utils;

import java.math.RoundingMode;
import java.util.UUID;

import org.decimal4j.util.DoubleRounder;

public class RandomUtils {
	public static double randomDouble() {
		return randomDouble(0, Double.MAX_VALUE);
	}

	public static double randomDouble(double max) {
		return randomDouble(0, max);
	}

	public static double randomDouble(double min, double max) {
		double newD = DoubleRounder.round(min + (Math.random() * (max - min)), 2, RoundingMode.UP);
		return newD;
	}

	public static String randomString(String str) {
		return str + "_" + randomInt();
	}

	public static int randomInt() {
		return randomInt(Integer.MAX_VALUE - 1);
	}

	public static int randomInt(int max) {
		return randomInt(0, max);
	}

	public static int randomInt(int min, int max) {
		return min + (int) (Math.random() * (max + 1 - min));
	}

	public static String randomId() {
		return UUID.randomUUID().toString();
	}
}
