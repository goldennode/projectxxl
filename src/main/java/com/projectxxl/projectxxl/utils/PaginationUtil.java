package com.projectxxl.projectxxl.utils;

import java.util.Arrays;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;

public final class PaginationUtil {
	private PaginationUtil() {
		throw new AssertionError();
	}

	public static <T> PageData<T> paginate(int page, int pageSize, T[] widgetEntities) throws PageNotFoundException {
		// For size=10: 0,10,20
		int totalPages = widgetEntities.length / pageSize;
		if (page > totalPages)
			throw new PageNotFoundException("Page " + page + " doesn't exist");
		int beginIndex = page * pageSize > (widgetEntities.length - 1) ? 0 : page * pageSize;
		// For size=10 : 9,19,29
		int endIndex = beginIndex + pageSize > widgetEntities.length ? widgetEntities.length : beginIndex + pageSize;
		Arrays.sort(widgetEntities);
		T[] pagedEntities = Arrays.copyOfRange(widgetEntities, beginIndex, endIndex);
		return new PageData<T>(pagedEntities, page, totalPages, pageSize);
	}
}