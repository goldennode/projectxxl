package com.projectxxl.projectxxl.domain;

public class PageData<T> {
	private final T[] entityArray;
	private final int maxPages;
	private final int pageSize;
	private final int page;

	public PageData(T[] entityArray, int page, int maxPages, int pageSize) {
		this.entityArray = entityArray;
		this.page = page;
		this.maxPages = maxPages;
		this.pageSize = pageSize;
	}

	public T[] getEntityArray() {
		return entityArray;
	}

	public int getMaxPages() {
		return maxPages;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getPage() {
		return page;
	}
}