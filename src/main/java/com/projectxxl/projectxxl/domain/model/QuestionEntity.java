package com.projectxxl.projectxxl.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.projectxxl.projectxxl.utils.RandomUtils;

@Entity(name = "questions")
public class QuestionEntity extends BaseEntity {
	@Column(nullable = false, length = 50)
	private final String user_id;
	@Column(nullable = false, columnDefinition = "TEXT")
	private final String question;

	public QuestionEntity() {
		super();
		this.user_id = null;
		this.question = null;
	}

	public QuestionEntity(Builder builder) {
		super(builder);
		this.user_id = builder.user_id;
		this.question = builder.question;
	}

	public String getId() {
		return id;
	}

	public String getUser_id() {
		return user_id;
	}

	public String getQuestion() {
		return question;
	}

	public Builder toBuilder() {
		return new QuestionEntity.Builder(this);
	}

	public static QuestionEntity createRandom() {
		return new QuestionEntity.Builder().withQuestion(RandomUtils.randomString("question")).withUser_id(RandomUtils.randomString("user_id")).withId(RandomUtils.randomId())
				.withCreatedAt((long) RandomUtils.randomInt()).withModifiedAt((long) RandomUtils.randomInt()).build();
	}

	public static class Builder extends BaseEntityBuilder {
		private String user_id;
		private String question;

		public Builder(QuestionEntity entity) {
			super(entity);
			this.user_id = entity.user_id;
			this.question = entity.question;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public String getUser_id() {
			return user_id;
		}

		public Builder withUser_id(String user_id) {
			this.user_id = user_id;
			return this;
		}

		public String getQuestion() {
			return question;
		}

		public Builder withQuestion(String question) {
			this.question = question;
			return this;
		}

		public QuestionEntity build() {
			return new QuestionEntity(this);
		}
	}
}
