package com.projectxxl.projectxxl.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.projectxxl.projectxxl.utils.RandomUtils;

@Entity(name = "transactions")
public class TransactionEntity extends BaseEntity {
	@Column(nullable = false, length = 50)
	private final Integer type;
	@Column(nullable = false, length = 50)
	private final String answer_id;
	@Column(nullable = false, length = 50)
	private final String question_id;
	@Column(nullable = false, precision = 16, scale = 2)
	private final Double amount;
	@Column(nullable = false, precision = 1, scale = 0)
	private final Integer status;

	public TransactionEntity() {
		super();
		this.type = null;
		this.answer_id = null;
		this.question_id = null;
		this.amount = null;
		this.status = null;
	}

	public TransactionEntity(Builder builder) {
		super(builder);
		this.type = builder.type;
		this.answer_id = builder.answer_id;
		this.question_id = builder.question_id;
		this.amount = builder.amount;
		this.status = builder.status;
	}

	public String getId() {
		return id;
	}

	public Integer getType() {
		return type;
	}

	public String getAnswer_id() {
		return answer_id;
	}

	public String getQuestion_id() {
		return question_id;
	}

	public double getAmount() {
		return amount;
	}

	public Integer getStatus() {
		return status;
	}

	public Builder toBuilder() {
		return new TransactionEntity.Builder(this);
	}

	public static TransactionEntity createRandom() {
		return new TransactionEntity.Builder().withAmount(RandomUtils.randomDouble()).withAnswer_id(RandomUtils.randomString("answer_id"))
				.withQuestion_id(RandomUtils.randomString("question_id")).withStatus(RandomUtils.randomInt(5)).withType(RandomUtils.randomInt(5)).withId(RandomUtils.randomId())
				.withCreatedAt((long) RandomUtils.randomInt()).withModifiedAt((long) RandomUtils.randomInt()).build();
	}

	public static class Builder extends BaseEntityBuilder {
		private Integer type;
		private String answer_id;
		private String question_id;
		private Double amount;
		private Integer status;

		public Builder(TransactionEntity entity) {
			super(entity);
			this.type = entity.type;
			this.answer_id = entity.answer_id;
			this.question_id = entity.question_id;
			this.amount = entity.amount;
			this.status = entity.status;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public Integer getType() {
			return type;
		}

		public Builder withType(Integer type) {
			this.type = type;
			return this;
		}

		public String getAnswer_id() {
			return answer_id;
		}

		public Builder withAnswer_id(String answer_id) {
			this.answer_id = answer_id;
			return this;
		}

		public String getQuestion_id() {
			return question_id;
		}

		public Builder withQuestion_id(String question_id) {
			this.question_id = question_id;
			return this;
		}

		public Double getAmount() {
			return amount;
		}

		public Builder withAmount(Double amount) {
			this.amount = amount;
			return this;
		}

		public Integer getStatus() {
			return status;
		}

		public Builder withStatus(Integer status) {
			this.status = status;
			return this;
		}

		public TransactionEntity build() {
			return new TransactionEntity(this);
		}
	}
}
