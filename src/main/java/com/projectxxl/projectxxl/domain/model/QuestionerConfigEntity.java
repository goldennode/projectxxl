package com.projectxxl.projectxxl.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.projectxxl.projectxxl.utils.RandomUtils;

@Entity(name = "questioner_config")
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "user_id" }) })
public class QuestionerConfigEntity extends BaseEntity {
	@Column(nullable = false, length = 50)
	private final String user_id;
	@Column(nullable = false, precision = 16, scale = 2)
	private final Double cost_min;
	@Column(nullable = false, precision = 16, scale = 2)
	private final Double cost_max;
	@Column(nullable = false, precision = 1, scale = 1)
	private final Double answerer_rating_min;
	@Column(nullable = false, precision = 1, scale = 0)
	private final Integer answerer_norating_acceptable;

	public QuestionerConfigEntity() {
		super();
		this.user_id = null;
		this.cost_min = null;
		this.cost_max = null;
		this.answerer_rating_min = null;
		this.answerer_norating_acceptable = null;
	}

	public QuestionerConfigEntity(Builder builder) {
		super(builder);
		this.user_id = builder.user_id;
		this.cost_min = builder.cost_min;
		this.cost_max = builder.cost_max;
		this.answerer_rating_min = builder.answerer_rating_min;
		this.answerer_norating_acceptable = builder.answerer_norating_acceptable;
	}

	public String getId() {
		return id;
	}

	public String getUser_id() {
		return user_id;
	}

	public double getCost_min() {
		return cost_min;
	}

	public double getCost_max() {
		return cost_max;
	}

	public double getAnswerer_rating_min() {
		return answerer_rating_min;
	}

	public int getAnswerer_norating_acceptable() {
		return answerer_norating_acceptable;
	}

	public Builder toBuilder() {
		return new QuestionerConfigEntity.Builder(this);
	}

	public static QuestionerConfigEntity createRandom() {
		return new Builder().withAnswerer_rating_min(RandomUtils.randomDouble(5)).withAnswerer_norating_acceptable(RandomUtils.randomInt(1))
				.withCost_min(RandomUtils.randomDouble(1, 5)).withCost_max(RandomUtils.randomDouble(10, 100)).withUser_id(RandomUtils.randomString("user_id"))
				.withId(RandomUtils.randomId()).withCreatedAt((long) RandomUtils.randomInt()).withModifiedAt((long) RandomUtils.randomInt()).build();
	}

	public static class Builder extends BaseEntityBuilder {
		private String user_id;
		private Double cost_min;
		private Double cost_max;
		private Double answerer_rating_min;
		private Integer answerer_norating_acceptable;

		public Builder(QuestionerConfigEntity entity) {
			super(entity);
			this.user_id = entity.user_id;
			this.cost_min = entity.cost_min;
			this.cost_max = entity.cost_max;
			this.answerer_rating_min = entity.answerer_rating_min;
			this.answerer_norating_acceptable = entity.answerer_norating_acceptable;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public String getUser_id() {
			return user_id;
		}

		public Builder withUser_id(String user_id) {
			this.user_id = user_id;
			return this;
		}

		public Double getCost_min() {
			return cost_min;
		}

		public Builder withCost_min(Double cost_min) {
			this.cost_min = cost_min;
			return this;
		}

		public Double getCost_max() {
			return cost_max;
		}

		public Builder withCost_max(Double cost_max) {
			this.cost_max = cost_max;
			return this;
		}

		public Double getAnswerer_rating_min() {
			return answerer_rating_min;
		}

		public Builder withAnswerer_rating_min(Double answerer_rating_min) {
			this.answerer_rating_min = answerer_rating_min;
			return this;
		}

		public Integer getAnswerer_norating_acceptable() {
			return answerer_norating_acceptable;
		}

		public Builder withAnswerer_norating_acceptable(Integer answerer_norating_acceptable) {
			this.answerer_norating_acceptable = answerer_norating_acceptable;
			return this;
		}

		public QuestionerConfigEntity build() {
			return new QuestionerConfigEntity(this);
		}
	}
}
