package com.projectxxl.projectxxl.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.projectxxl.projectxxl.utils.RandomUtils;

@Entity(name = "users")
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "user_unique_id" }), @UniqueConstraint(columnNames = { "access_key" }) })
public class UserEntity extends BaseEntity {
	@Column(nullable = false, length = 50)
	private final String full_name;
	@Column(nullable = false, length = 50)
	private final String user_unique_id;
	@Column(nullable = false, length = 50)
	private final String password;
	@Column(nullable = false, length = 50)
	private final String user_name;
	@Column(nullable = false, length = 50)
	private final String access_key;
	@Column(nullable = false, length = 50)
	private final String secret_key;

	public UserEntity() {
		super();
		this.full_name = null;
		this.user_unique_id = null;
		this.password = null;
		this.user_name = null;
		this.access_key = null;
		this.secret_key = null;
	}

	public UserEntity(Builder builder) {
		super(builder);
		this.full_name = builder.full_name;
		this.user_unique_id = builder.user_unique_id;
		this.password = builder.password;
		this.user_name = builder.user_name;
		this.access_key = builder.access_key;
		this.secret_key = builder.secret_key;
	}

	public String getId() {
		return id;
	}

	public String getFull_name() {
		return full_name;
	}

	public String getUser_unique_id() {
		return user_unique_id;
	}

	public String getPassword() {
		return password;
	}

	public String getUser_name() {
		return user_name;
	}

	public String getAccess_key() {
		return access_key;
	}

	public String getSecret_key() {
		return secret_key;
	}

	public Builder toBuilder() {
		return new UserEntity.Builder(this);
	}

	public static UserEntity createRandom() {
		return new UserEntity.Builder().withFull_name(RandomUtils.randomString("full_name")).withPassword(RandomUtils.randomString("password"))
				.withUser_name(RandomUtils.randomString("user_name")).withUser_unique_id(RandomUtils.randomString("user_unique_id"))
				.withAccess_key(RandomUtils.randomString("access_key")).withSecret_key(RandomUtils.randomString("secret_key")).withId(RandomUtils.randomId())
				.withCreatedAt((long) RandomUtils.randomInt()).withModifiedAt((long) RandomUtils.randomInt()).build();
	}

	public static class Builder extends BaseEntityBuilder {
		private String full_name;
		private String user_unique_id;
		private String password;
		private String user_name;
		private String access_key;
		private String secret_key;

		public Builder(UserEntity entity) {
			super(entity);
			this.full_name = entity.full_name;
			this.user_unique_id = entity.user_unique_id;
			this.password = entity.password;
			this.user_name = entity.user_name;
			this.access_key = entity.access_key;
			this.secret_key = entity.secret_key;
		}

		public Builder() {
			//
		}

		public String getId() {
			return id;
		}

		public Builder withId(String id) {
			this.id = id;
			return this;
		}

		public Long getCreatedAt() {
			return createdAt;
		}

		public Builder withCreatedAt(Long createdAt) {
			this.createdAt = createdAt;
			return this;
		}

		public Long getModifiedAt() {
			return modifiedAt;
		}

		public Builder withModifiedAt(Long modifiedAt) {
			this.modifiedAt = modifiedAt;
			return this;
		}

		public String getFull_name() {
			return full_name;
		}

		public Builder withFull_name(String full_name) {
			this.full_name = full_name;
			return this;
		}

		public String getUser_unique_id() {
			return user_unique_id;
		}

		public Builder withUser_unique_id(String user_unique_id) {
			this.user_unique_id = user_unique_id;
			return this;
		}

		public String getPassword() {
			return password;
		}

		public Builder withPassword(String password) {
			this.password = password;
			return this;
		}

		public String getUser_name() {
			return user_name;
		}

		public Builder withUser_name(String user_name) {
			this.user_name = user_name;
			return this;
		}

		public String getAccess_key() {
			return access_key;
		}

		public Builder withAccess_key(String access_key) {
			this.access_key = access_key;
			return this;
		}

		public String getSecret_key() {
			return secret_key;
		}

		public Builder withSecret_key(String secret_key) {
			this.secret_key = secret_key;
			return this;
		}

		public UserEntity build() {
			return new UserEntity(this);
		}
	}
}
