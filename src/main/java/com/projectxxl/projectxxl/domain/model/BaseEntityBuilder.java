package com.projectxxl.projectxxl.domain.model;

public abstract class BaseEntityBuilder {
	protected String id;
	protected Long createdAt;
	protected Long modifiedAt;

	public BaseEntityBuilder() {
	}

	public BaseEntityBuilder(BaseEntity entity) {
		this.id = entity.id;
		this.createdAt = entity.createdAt;
		this.modifiedAt = entity.modifiedAt;
	}

	public String getId() {
		return id;
	}

	public BaseEntityBuilder withId(String id) {
		this.id = id;
		return this;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public BaseEntityBuilder withCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	public Long getModifiedAt() {
		return modifiedAt;
	}

	public BaseEntityBuilder withModifiedAt(Long modifiedAt) {
		this.modifiedAt = modifiedAt;
		return this;
	}
}
