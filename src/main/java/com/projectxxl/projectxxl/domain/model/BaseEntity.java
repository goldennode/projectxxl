package com.projectxxl.projectxxl.domain.model;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity implements Comparable<BaseEntity> {
	@Id
	@Column(nullable = false, length = 50)
	protected final String id;
	@Column(nullable = false, precision = 13, scale = 0)
	protected final Long createdAt;
	@Column(nullable = false, precision = 13, scale = 0)
	protected final Long modifiedAt;

	public BaseEntity(BaseEntityBuilder builder) {
		this.id = builder.id;
		this.createdAt = builder.createdAt;
		this.modifiedAt = builder.modifiedAt;
	}

	public BaseEntity() {
		this.id = null;
		this.createdAt = null;
		this.modifiedAt = null;
	}

	public String getId() {
		return id;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public Long getModifiedAt() {
		return modifiedAt;
	}

	@Override
	public int compareTo(BaseEntity o) {
		if (o == null)
			return 1;
		if (this.createdAt == null) {
			if (o.createdAt == null) {
				return this.id.compareTo(o.id);
			} else {
				return 1;
			}
		} else {
			if (o.createdAt == null) {
				return -1;
			} else {
				int comparisonResult = this.createdAt.compareTo(o.createdAt);
				if (comparisonResult == 0)
					return this.id.compareTo(o.id);
				else
					return comparisonResult;
			}
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseEntity other = (BaseEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
