package com.projectxxl.projectxxl.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.projectxxl.projectxxl.domain.model.QuestionEntity;

public interface QuestionRepository extends CrudRepository<QuestionEntity, String> {
}