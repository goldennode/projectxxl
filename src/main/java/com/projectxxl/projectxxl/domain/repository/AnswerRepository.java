package com.projectxxl.projectxxl.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.projectxxl.projectxxl.domain.model.AnswerEntity;

public interface AnswerRepository extends CrudRepository<AnswerEntity, String> {
}