package com.projectxxl.projectxxl.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.projectxxl.projectxxl.domain.model.TransactionEntity;

public interface TransactionRepository extends CrudRepository<TransactionEntity, String> {
}