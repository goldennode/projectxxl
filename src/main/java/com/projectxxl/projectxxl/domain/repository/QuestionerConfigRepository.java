package com.projectxxl.projectxxl.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.projectxxl.projectxxl.domain.model.QuestionerConfigEntity;

public interface QuestionerConfigRepository extends CrudRepository<QuestionerConfigEntity, String> {
}