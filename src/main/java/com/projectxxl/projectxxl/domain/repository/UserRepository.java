package com.projectxxl.projectxxl.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.projectxxl.projectxxl.domain.model.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, String> {
}