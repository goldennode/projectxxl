package com.projectxxl.projectxxl.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.projectxxl.projectxxl.domain.model.AnswererConfigEntity;

public interface AnswererConfigRepository extends CrudRepository<AnswererConfigEntity, String> {
}