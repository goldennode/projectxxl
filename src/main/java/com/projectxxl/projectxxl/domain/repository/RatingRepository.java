package com.projectxxl.projectxxl.domain.repository;

import org.springframework.data.repository.CrudRepository;

import com.projectxxl.projectxxl.domain.model.RatingEntity;

public interface RatingRepository extends CrudRepository<RatingEntity, String> {
}