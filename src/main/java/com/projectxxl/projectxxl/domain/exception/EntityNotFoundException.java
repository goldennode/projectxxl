package com.projectxxl.projectxxl.domain.exception;

public class EntityNotFoundException extends Exception {
	public EntityNotFoundException(String string) {
		super(string);
	}
}