package com.projectxxl.projectxxl.domain.exception;

public class PageNotFoundException extends Exception {
	public PageNotFoundException(String string) {
		super(string);
	}
}