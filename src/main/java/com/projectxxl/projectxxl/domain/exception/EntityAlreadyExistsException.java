package com.projectxxl.projectxxl.domain.exception;

public class EntityAlreadyExistsException extends Exception {
	public EntityAlreadyExistsException(String string) {
		super(string);
	}
}