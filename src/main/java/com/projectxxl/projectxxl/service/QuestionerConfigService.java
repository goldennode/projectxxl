package com.projectxxl.projectxxl.service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.QuestionerConfigEntity;

public interface QuestionerConfigService {
	QuestionerConfigEntity createEntity(QuestionerConfigEntity entity) throws EntityAlreadyExistsException;

	QuestionerConfigEntity updateEntity(QuestionerConfigEntity entity) throws EntityNotFoundException;

	QuestionerConfigEntity getEntity(String id) throws EntityNotFoundException;

	void deleteEntity(String id) throws EntityNotFoundException;

	PageData<QuestionerConfigEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException;
}
