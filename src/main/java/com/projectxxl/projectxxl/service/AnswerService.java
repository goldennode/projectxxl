package com.projectxxl.projectxxl.service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.AnswerEntity;

public interface AnswerService {
	AnswerEntity createEntity(AnswerEntity entity) throws EntityAlreadyExistsException;

	AnswerEntity updateEntity(AnswerEntity entity) throws EntityNotFoundException;

	AnswerEntity getEntity(String id) throws EntityNotFoundException;

	void deleteEntity(String id) throws EntityNotFoundException;

	PageData<AnswerEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException;
}
