package com.projectxxl.projectxxl.service;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.TransactionEntity;
import com.projectxxl.projectxxl.domain.repository.TransactionRepository;
import com.projectxxl.projectxxl.utils.PaginationUtil;

@Service
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	private TransactionRepository repository;

	public TransactionEntity createEntity(TransactionEntity entity) throws EntityAlreadyExistsException {
		return repository.save(((TransactionEntity.Builder) ServiceUtils.processCreateBuilder(entity.toBuilder())).build());
	}

	public TransactionEntity updateEntity(TransactionEntity entity) throws EntityNotFoundException {
		return repository.save(((TransactionEntity.Builder) ServiceUtils.processUpdateBuilder(entity.toBuilder())).build());
	}

	public TransactionEntity getEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			return repository.findById(id).get();
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public void deleteEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			repository.deleteById(id);
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public PageData<TransactionEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException {
		List<TransactionEntity> entities = new ArrayList<TransactionEntity>();
		repository.findAll().forEach(entity -> entities.add(entity));
		TransactionEntity[] entityArray = entities.toArray(new TransactionEntity[0]);
		return PaginationUtil.paginate(page, pageSize, entityArray);
	}
}
