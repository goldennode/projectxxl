package com.projectxxl.projectxxl.service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.QuestionEntity;

public interface QuestionService {
	QuestionEntity createEntity(QuestionEntity entity) throws EntityAlreadyExistsException;

	QuestionEntity updateEntity(QuestionEntity entity) throws EntityNotFoundException;

	QuestionEntity getEntity(String id) throws EntityNotFoundException;

	void deleteEntity(String id) throws EntityNotFoundException;

	PageData<QuestionEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException;
}
