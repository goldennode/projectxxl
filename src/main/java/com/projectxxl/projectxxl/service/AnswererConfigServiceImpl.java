package com.projectxxl.projectxxl.service;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.AnswererConfigEntity;
import com.projectxxl.projectxxl.domain.repository.AnswererConfigRepository;
import com.projectxxl.projectxxl.utils.PaginationUtil;

@Service
public class AnswererConfigServiceImpl implements AnswererConfigService {
	@Autowired
	private AnswererConfigRepository repository;

	public AnswererConfigEntity createEntity(AnswererConfigEntity entity) throws EntityAlreadyExistsException {
		return repository.save(((AnswererConfigEntity.Builder) ServiceUtils.processCreateBuilder(entity.toBuilder())).build());
	}

	public AnswererConfigEntity updateEntity(AnswererConfigEntity entity) throws EntityNotFoundException {
		return repository.save(((AnswererConfigEntity.Builder) ServiceUtils.processUpdateBuilder(entity.toBuilder())).build());
	}

	public AnswererConfigEntity getEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			return repository.findById(id).get();
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public void deleteEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			repository.deleteById(id);
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public PageData<AnswererConfigEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException {
		List<AnswererConfigEntity> entities = new ArrayList<AnswererConfigEntity>();
		repository.findAll().forEach(entity -> entities.add(entity));
		AnswererConfigEntity[] entityArray = entities.toArray(new AnswererConfigEntity[0]);
		return PaginationUtil.paginate(page, pageSize, entityArray);
	}
}
