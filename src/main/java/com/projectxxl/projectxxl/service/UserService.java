package com.projectxxl.projectxxl.service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.UserEntity;

public interface UserService {
	UserEntity createEntity(UserEntity entity) throws EntityAlreadyExistsException;

	UserEntity updateEntity(UserEntity entity) throws EntityNotFoundException;

	UserEntity getEntity(String id) throws EntityNotFoundException;

	void deleteEntity(String id) throws EntityNotFoundException;

	PageData<UserEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException;
}