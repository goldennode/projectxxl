package com.projectxxl.projectxxl.service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.TransactionEntity;

public interface TransactionService {
	TransactionEntity createEntity(TransactionEntity entity) throws EntityAlreadyExistsException;

	TransactionEntity updateEntity(TransactionEntity entity) throws EntityNotFoundException;

	TransactionEntity getEntity(String id) throws EntityNotFoundException;

	void deleteEntity(String id) throws EntityNotFoundException;

	PageData<TransactionEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException;;
}
