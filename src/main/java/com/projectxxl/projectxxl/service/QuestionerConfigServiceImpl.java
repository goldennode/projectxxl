package com.projectxxl.projectxxl.service;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.QuestionerConfigEntity;
import com.projectxxl.projectxxl.domain.repository.QuestionerConfigRepository;
import com.projectxxl.projectxxl.utils.PaginationUtil;

@Service
public class QuestionerConfigServiceImpl implements QuestionerConfigService {
	@Autowired
	private QuestionerConfigRepository repository;

	public QuestionerConfigEntity createEntity(QuestionerConfigEntity entity) throws EntityAlreadyExistsException {
		return repository.save(((QuestionerConfigEntity.Builder) ServiceUtils.processCreateBuilder(entity.toBuilder())).build());
	}

	public QuestionerConfigEntity updateEntity(QuestionerConfigEntity entity) throws EntityNotFoundException {
		return repository.save(((QuestionerConfigEntity.Builder) ServiceUtils.processUpdateBuilder(entity.toBuilder())).build());
	}

	public QuestionerConfigEntity getEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			return repository.findById(id).get();
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public void deleteEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			repository.deleteById(id);
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public PageData<QuestionerConfigEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException {
		List<QuestionerConfigEntity> entities = new ArrayList<QuestionerConfigEntity>();
		repository.findAll().forEach(entity -> entities.add(entity));
		QuestionerConfigEntity[] entityArray = entities.toArray(new QuestionerConfigEntity[0]);
		return PaginationUtil.paginate(page, pageSize, entityArray);
	}
}
