package com.projectxxl.projectxxl.service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.RatingEntity;

public interface RatingService {
	RatingEntity createEntity(RatingEntity entity) throws EntityAlreadyExistsException;

	RatingEntity updateEntity(RatingEntity entity) throws EntityNotFoundException;

	RatingEntity getEntity(String id) throws EntityNotFoundException;

	void deleteEntity(String id) throws EntityNotFoundException;

	PageData<RatingEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException;
}
