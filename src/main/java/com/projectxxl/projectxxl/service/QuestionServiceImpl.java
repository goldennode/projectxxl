package com.projectxxl.projectxxl.service;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.QuestionEntity;
import com.projectxxl.projectxxl.domain.repository.QuestionRepository;
import com.projectxxl.projectxxl.utils.PaginationUtil;

@Service
public class QuestionServiceImpl implements QuestionService {
	@Autowired
	private QuestionRepository repository;

	public QuestionEntity createEntity(QuestionEntity entity) throws EntityAlreadyExistsException {
		return repository.save(((QuestionEntity.Builder) ServiceUtils.processCreateBuilder(entity.toBuilder())).build());
	}

	public QuestionEntity updateEntity(QuestionEntity entity) throws EntityNotFoundException {
		return repository.save(((QuestionEntity.Builder) ServiceUtils.processUpdateBuilder(entity.toBuilder())).build());
	}

	public QuestionEntity getEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			return repository.findById(id).get();
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public void deleteEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			repository.deleteById(id);
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public PageData<QuestionEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException {
		List<QuestionEntity> entities = new ArrayList<QuestionEntity>();
		repository.findAll().forEach(entity -> entities.add(entity));
		QuestionEntity[] entityArray = entities.toArray(new QuestionEntity[0]);
		return PaginationUtil.paginate(page, pageSize, entityArray);
	}
}
