package com.projectxxl.projectxxl.service;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.AnswerEntity;
import com.projectxxl.projectxxl.domain.repository.AnswerRepository;
import com.projectxxl.projectxxl.utils.PaginationUtil;

@Service
public class AnswerServiceImpl implements AnswerService {
	@Autowired
	private AnswerRepository repository;

	public AnswerEntity createEntity(AnswerEntity entity) throws EntityAlreadyExistsException {
		return repository.save(((AnswerEntity.Builder) ServiceUtils.processCreateBuilder(entity.toBuilder())).build());
	}

	public AnswerEntity updateEntity(AnswerEntity entity) throws EntityNotFoundException {
		return repository.save(((AnswerEntity.Builder) ServiceUtils.processUpdateBuilder(entity.toBuilder())).build());
	}

	public AnswerEntity getEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			return repository.findById(id).get();
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public void deleteEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			repository.deleteById(id);
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public PageData<AnswerEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException {
		List<AnswerEntity> entities = new ArrayList<AnswerEntity>();
		repository.findAll().forEach(entity -> entities.add(entity));
		AnswerEntity[] entityArray = entities.toArray(new AnswerEntity[0]);
		return PaginationUtil.paginate(page, pageSize, entityArray);
	}
}
