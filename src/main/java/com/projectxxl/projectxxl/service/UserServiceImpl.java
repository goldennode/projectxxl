package com.projectxxl.projectxxl.service;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.UserEntity;
import com.projectxxl.projectxxl.domain.repository.UserRepository;
import com.projectxxl.projectxxl.utils.PaginationUtil;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository repository;

	public UserEntity createEntity(UserEntity entity) throws EntityAlreadyExistsException {
		return repository.save(((UserEntity.Builder) ServiceUtils.processCreateBuilder(entity.toBuilder())).build());
	}

	public UserEntity updateEntity(UserEntity entity) throws EntityNotFoundException {
		return repository.save(((UserEntity.Builder) ServiceUtils.processUpdateBuilder(entity.toBuilder())).build());
	}

	public UserEntity getEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			return repository.findById(id).get();
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public void deleteEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			repository.deleteById(id);
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public PageData<UserEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException {
		List<UserEntity> entities = new ArrayList<UserEntity>();
		repository.findAll().forEach(entity -> entities.add(entity));
		UserEntity[] entityArray = entities.toArray(new UserEntity[0]);
		return PaginationUtil.paginate(page, pageSize, entityArray);
	}
}