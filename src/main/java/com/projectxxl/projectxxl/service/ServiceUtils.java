package com.projectxxl.projectxxl.service;

import static com.google.common.base.Preconditions.checkArgument;

import com.projectxxl.projectxxl.domain.model.BaseEntityBuilder;
import com.projectxxl.projectxxl.utils.RandomUtils;

public class ServiceUtils {
	public static BaseEntityBuilder processCreateBuilder(BaseEntityBuilder builder) {
		checkArgument(builder.getId() == null, "Entity to be created should not contain id");
		BaseEntityBuilder updatedBuilder = builder.withId(RandomUtils.randomId()).withCreatedAt(System.currentTimeMillis()).withModifiedAt(System.currentTimeMillis());
		return updatedBuilder;
	}

	public static BaseEntityBuilder processUpdateBuilder(BaseEntityBuilder builder) {
		checkArgument(builder.getId() != null, "Entity to be updated should contain id");
		BaseEntityBuilder updatedBuilder = builder.withModifiedAt(System.currentTimeMillis());
		return updatedBuilder;
	}
}
