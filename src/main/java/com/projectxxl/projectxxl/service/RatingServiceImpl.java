package com.projectxxl.projectxxl.service;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.RatingEntity;
import com.projectxxl.projectxxl.domain.repository.RatingRepository;
import com.projectxxl.projectxxl.utils.PaginationUtil;

@Service
public class RatingServiceImpl implements RatingService {
	@Autowired
	private RatingRepository repository;

	public RatingEntity createEntity(RatingEntity entity) throws EntityAlreadyExistsException {
		return repository.save(((RatingEntity.Builder) ServiceUtils.processCreateBuilder(entity.toBuilder())).build());
	}

	public RatingEntity updateEntity(RatingEntity entity) throws EntityNotFoundException {
		return repository.save(((RatingEntity.Builder) ServiceUtils.processUpdateBuilder(entity.toBuilder())).build());
	}

	public RatingEntity getEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			return repository.findById(id).get();
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public void deleteEntity(String id) throws EntityNotFoundException {
		checkArgument(id != null, "id shouldn't be null");
		try {
			repository.deleteById(id);
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("Entity not found with id : " + id);
		}
	}

	public PageData<RatingEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException {
		List<RatingEntity> entities = new ArrayList<RatingEntity>();
		repository.findAll().forEach(entity -> entities.add(entity));
		RatingEntity[] entityArray = entities.toArray(new RatingEntity[0]);
		return PaginationUtil.paginate(page, pageSize, entityArray);
	}
}
