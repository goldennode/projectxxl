package com.projectxxl.projectxxl.service;

import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.exception.EntityNotFoundException;
import com.projectxxl.projectxxl.domain.exception.PageNotFoundException;
import com.projectxxl.projectxxl.domain.model.AnswererConfigEntity;

public interface AnswererConfigService {
	AnswererConfigEntity createEntity(AnswererConfigEntity entity) throws EntityAlreadyExistsException;

	AnswererConfigEntity updateEntity(AnswererConfigEntity entity) throws EntityNotFoundException;

	AnswererConfigEntity getEntity(String id) throws EntityNotFoundException;

	void deleteEntity(String id) throws EntityNotFoundException;

	PageData<AnswererConfigEntity> getAllEntities(int page, int pageSize) throws PageNotFoundException;
}
