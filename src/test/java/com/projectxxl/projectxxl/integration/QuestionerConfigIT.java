package com.projectxxl.projectxxl.integration;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectxxl.projectxxl.controllers.dto.QuestionerConfigDTO;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.model.QuestionerConfigEntity;
import com.projectxxl.projectxxl.domain.repository.QuestionerConfigRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class QuestionerConfigIT {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private QuestionerConfigRepository repository;
	@Autowired
	private ObjectMapper objectMapper;
	private QuestionerConfigEntity[] testEntities;

	@BeforeEach
	public void init() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void givenEntities_whenGetEntity_thenStatus200() throws Exception {
		createTestEntities(5);
		mockMvc.perform(get("/questionerconfig/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(testEntities[0].getId()))).andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
				.andExpect(jsonPath("$.modifiedAt", is(testEntities[0].getModifiedAt()), Long.class))
				.andExpect(jsonPath("$.answerer_norating_acceptable", is(testEntities[0].getAnswerer_norating_acceptable())))
				.andExpect(jsonPath("$.answerer_rating_min", is(testEntities[0].getAnswerer_rating_min()))).andExpect(jsonPath("$.cost_max", is(testEntities[0].getCost_max())))
				.andExpect(jsonPath("$.cost_min", is(testEntities[0].getCost_min()))).andExpect(jsonPath("$.user_id", is(testEntities[0].getUser_id()))).andReturn();
	}

	@Test
	public void givenNoEntities_whenCreateEntities_thenStatus201_widgetsReturnedHasSize1() throws Exception {
		QuestionerConfigDTO dto = QuestionerConfigDTO.createRandom();
		mockMvc.perform(post("/questionerconfig").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(any(String.class)))).andExpect(jsonPath("$.createdAt", is(any(Long.class)), Long.class))
				.andExpect(jsonPath("$.modifiedAt", is(any(Long.class)), Long.class))
				.andExpect(jsonPath("$.answerer_norating_acceptable", is(dto.getAnswerer_norating_acceptable())))
				.andExpect(jsonPath("$.answerer_rating_min", is(dto.getAnswerer_rating_min()))).andExpect(jsonPath("$.cost_max", is(dto.getCost_max())))
				.andExpect(jsonPath("$.cost_min", is(dto.getCost_min()))).andExpect(jsonPath("$.user_id", is(dto.getUser_id()))).andReturn();
		mockMvc.perform(get("/questionerconfig").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntity_thenStatus200() throws Exception {
		createTestEntities(2);
		QuestionerConfigDTO dto = QuestionerConfigDTO.createRandom();
		mockMvc.perform(put("/questionerconfig/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(testEntities[0].getId())))
				.andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
				.andExpect(jsonPath("$.modifiedAt", not(testEntities[0].getModifiedAt()), Long.class))
				.andExpect(jsonPath("$.answerer_norating_acceptable", is(dto.getAnswerer_norating_acceptable())))
				.andExpect(jsonPath("$.answerer_rating_min", is(dto.getAnswerer_rating_min()))).andExpect(jsonPath("$.cost_max", is(dto.getCost_max())))
				.andExpect(jsonPath("$.cost_min", is(dto.getCost_min()))).andExpect(jsonPath("$.user_id", is(dto.getUser_id()))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		QuestionerConfigDTO dto = QuestionerConfigDTO.createRandom();
		mockMvc.perform(put("/questionerconfig/{id}", 0).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto)))
				.andExpect(status().is4xxClientError()).andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/questionerconfig/{id}", 0).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError()).andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntity_thenStatus200_widgetsReturnedHasSize1() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/questionerconfig/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent()).andReturn();
		mockMvc.perform(get("/questionerconfig").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(testEntities[1].getId()))).andReturn();
	}

	private void createTestEntities(int count) throws EntityAlreadyExistsException {
		testEntities = new QuestionerConfigEntity[count];
		for (int i = 0; i < count; i++) {
			testEntities[i] = QuestionerConfigEntity.createRandom();
			repository.save(testEntities[i]);
		}
	}
}