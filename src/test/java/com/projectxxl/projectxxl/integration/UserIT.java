package com.projectxxl.projectxxl.integration;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectxxl.projectxxl.controllers.dto.UserDTO;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.model.UserEntity;
import com.projectxxl.projectxxl.domain.repository.UserRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserIT {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private UserRepository repository;
	@Autowired
	private ObjectMapper objectMapper;
	private UserEntity[] testEntities;

	@BeforeEach
	public void init() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void givenEntities_whenGetEntity_thenStatus200() throws Exception {
		createTestEntities(5);
		mockMvc.perform(get("/users/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(testEntities[0].getId()))).andExpect(jsonPath("$.access_key", is(testEntities[0].getAccess_key())))
				.andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class)).andExpect(jsonPath("$.full_name", is(testEntities[0].getFull_name())))
				.andExpect(jsonPath("$.modifiedAt", is(testEntities[0].getModifiedAt()), Long.class)).andExpect(jsonPath("$.password", is(testEntities[0].getPassword())))
				.andExpect(jsonPath("$.secret_key", is(testEntities[0].getSecret_key()))).andExpect(jsonPath("$.user_name", is(testEntities[0].getUser_name())))
				.andExpect(jsonPath("$.user_unique_id", is(testEntities[0].getUser_unique_id()))).andReturn();
	}

	@Test
	public void givenNoEntities_whenCreateEntities_thenStatus201_widgetsReturnedHasSize1() throws Exception {
		UserDTO dto = UserDTO.createRandom();
		mockMvc.perform(post("/users").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(any(String.class)))).andExpect(jsonPath("$.access_key", is(dto.getAccess_key())))
				.andExpect(jsonPath("$.createdAt", is(any(Long.class)), Long.class)).andExpect(jsonPath("$.full_name", is(dto.getFull_name())))
				.andExpect(jsonPath("$.modifiedAt", is(any(Long.class)), Long.class)).andExpect(jsonPath("$.password", is(dto.getPassword())))
				.andExpect(jsonPath("$.secret_key", is(dto.getSecret_key()))).andExpect(jsonPath("$.user_name", is(dto.getUser_name())))
				.andExpect(jsonPath("$.user_unique_id", is(dto.getUser_unique_id()))).andReturn();
		mockMvc.perform(get("/users").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntity_thenStatus200() throws Exception {
		createTestEntities(2);
		UserDTO dto = UserDTO.createRandom();
		mockMvc.perform(put("/users/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(testEntities[0].getId()))).andExpect(jsonPath("$.access_key", is(dto.getAccess_key())))
				.andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class)).andExpect(jsonPath("$.full_name", is(dto.getFull_name())))
				.andExpect(jsonPath("$.modifiedAt", not(testEntities[0].getModifiedAt()), Long.class)).andExpect(jsonPath("$.password", is(dto.getPassword())))
				.andExpect(jsonPath("$.secret_key", is(dto.getSecret_key()))).andExpect(jsonPath("$.user_name", is(dto.getUser_name())))
				.andExpect(jsonPath("$.user_unique_id", is(dto.getUser_unique_id()))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		UserDTO dto = UserDTO.createRandom();
		mockMvc.perform(put("/users/{id}", 0).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().is4xxClientError())
				.andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/users/{id}", 0).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError()).andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntity_thenStatus200_widgetsReturnedHasSize1() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/users/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent()).andReturn();
		mockMvc.perform(get("/users").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(testEntities[1].getId()))).andReturn();
	}

	private void createTestEntities(int count) throws EntityAlreadyExistsException {
		testEntities = new UserEntity[count];
		for (int i = 0; i < count; i++) {
			testEntities[i] = UserEntity.createRandom();
			repository.save(testEntities[i]);
		}
	}
}