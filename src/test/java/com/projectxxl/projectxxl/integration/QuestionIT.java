package com.projectxxl.projectxxl.integration;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectxxl.projectxxl.controllers.dto.QuestionDTO;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.model.QuestionEntity;
import com.projectxxl.projectxxl.domain.repository.QuestionRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class QuestionIT {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private QuestionRepository repository;
	@Autowired
	private ObjectMapper objectMapper;
	private QuestionEntity[] testEntities;

	@BeforeEach
	public void init() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void givenEntities_whenGetEntity_thenStatus200() throws Exception {
		createTestEntities(5);
		mockMvc.perform(get("/questions/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(testEntities[0].getId()))).andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
				.andExpect(jsonPath("$.modifiedAt", is(testEntities[0].getModifiedAt()), Long.class)).andExpect(jsonPath("$.question", is(testEntities[0].getQuestion())))
				.andExpect(jsonPath("$.user_id", is(testEntities[0].getUser_id()))).andReturn();
	}

	@Test
	public void givenNoEntities_whenCreateEntities_thenStatus201_widgetsReturnedHasSize1() throws Exception {
		QuestionDTO dto = QuestionDTO.createRandom();
		mockMvc.perform(post("/questions").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(any(String.class)))).andExpect(jsonPath("$.createdAt", is(any(Long.class)), Long.class))
				.andExpect(jsonPath("$.modifiedAt", is(any(Long.class)), Long.class)).andExpect(jsonPath("$.question", is(dto.getQuestion())))
				.andExpect(jsonPath("$.user_id", is(dto.getUser_id()))).andReturn();
		mockMvc.perform(get("/questions").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntity_thenStatus200() throws Exception {
		createTestEntities(2);
		QuestionDTO dto = QuestionDTO.createRandom();
		mockMvc.perform(put("/questions/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(testEntities[0].getId())))
				.andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
				.andExpect(jsonPath("$.modifiedAt", not(testEntities[0].getModifiedAt()), Long.class)).andExpect(jsonPath("$.question", is(dto.getQuestion())))
				.andExpect(jsonPath("$.user_id", is(dto.getUser_id()))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		QuestionDTO dto = QuestionDTO.createRandom();
		mockMvc.perform(put("/questions/{id}", 0).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().is4xxClientError())
				.andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/questions/{id}", 0).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError()).andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntity_thenStatus200_widgetsReturnedHasSize1() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/questions/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent()).andReturn();
		mockMvc.perform(get("/questions").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(testEntities[1].getId()))).andReturn();
	}

	private void createTestEntities(int count) throws EntityAlreadyExistsException {
		testEntities = new QuestionEntity[count];
		for (int i = 0; i < count; i++) {
			testEntities[i] = QuestionEntity.createRandom();
			repository.save(testEntities[i]);
		}
	}
}