package com.projectxxl.projectxxl.integration;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectxxl.projectxxl.controllers.dto.RatingDTO;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.model.RatingEntity;
import com.projectxxl.projectxxl.domain.repository.RatingRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RatingIT {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private RatingRepository repository;
	@Autowired
	private ObjectMapper objectMapper;
	private RatingEntity[] testEntities;

	@BeforeEach
	public void init() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void givenEntities_whenGetEntity_thenStatus200() throws Exception {
		createTestEntities(5);
		mockMvc.perform(get("/ratings/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(testEntities[0].getId()))).andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
				.andExpect(jsonPath("$.modifiedAt", is(testEntities[0].getModifiedAt()), Long.class)).andExpect(jsonPath("$.mode", is(testEntities[0].getMode())))
				.andExpect(jsonPath("$.question_id", is(testEntities[0].getQuestion_id()))).andExpect(jsonPath("$.answer_id", is(testEntities[0].getAnswer_id())))
				.andExpect(jsonPath("$.rating_communication", is(testEntities[0].getRating_communication())))
				.andExpect(jsonPath("$.rating_knowledge", is(testEntities[0].getRating_knowledge())))
				.andExpect(jsonPath("$.rating_overall", is(testEntities[0].getRating_overall()))).andExpect(jsonPath("$.rating_response", is(testEntities[0].getRating_response())))
				.andExpect(jsonPath("$.rating_text", is(testEntities[0].getRating_text()))).andExpect(jsonPath("$.user_id_for", is(testEntities[0].getUser_id_for())))
				.andExpect(jsonPath("$.user_id_from", is(testEntities[0].getUser_id_from()))).andReturn();
	}

	@Test
	public void givenNoEntities_whenCreateEntities_thenStatus201_widgetsReturnedHasSize1() throws Exception {
		RatingDTO dto = RatingDTO.createRandom();
		mockMvc.perform(post("/ratings").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(any(String.class)))).andExpect(jsonPath("$.createdAt", is(any(Long.class)), Long.class))
				.andExpect(jsonPath("$.modifiedAt", is(any(Long.class)), Long.class)).andExpect(jsonPath("$.mode", is(dto.getMode())))
				.andExpect(jsonPath("$.question_id", is(dto.getQuestion_id()))).andExpect(jsonPath("$.answer_id", is(dto.getAnswer_id())))
				.andExpect(jsonPath("$.rating_communication", is(dto.getRating_communication()))).andExpect(jsonPath("$.rating_knowledge", is(dto.getRating_knowledge())))
				.andExpect(jsonPath("$.rating_overall", is(dto.getRating_overall()))).andExpect(jsonPath("$.rating_response", is(dto.getRating_response())))
				.andExpect(jsonPath("$.rating_text", is(dto.getRating_text()))).andExpect(jsonPath("$.user_id_for", is(dto.getUser_id_for())))
				.andExpect(jsonPath("$.user_id_from", is(dto.getUser_id_from()))).andReturn();
		mockMvc.perform(get("/ratings").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntity_thenStatus200() throws Exception {
		createTestEntities(2);
		RatingDTO dto = RatingDTO.createRandom();
		mockMvc.perform(put("/ratings/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(testEntities[0].getId())))
				.andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
				.andExpect(jsonPath("$.modifiedAt", not(testEntities[0].getModifiedAt()), Long.class)).andExpect(jsonPath("$.mode", is(dto.getMode())))
				.andExpect(jsonPath("$.question_id", is(dto.getQuestion_id()))).andExpect(jsonPath("$.answer_id", is(dto.getAnswer_id())))
				.andExpect(jsonPath("$.rating_communication", is(dto.getRating_communication()))).andExpect(jsonPath("$.rating_knowledge", is(dto.getRating_knowledge())))
				.andExpect(jsonPath("$.rating_overall", is(dto.getRating_overall()))).andExpect(jsonPath("$.rating_response", is(dto.getRating_response())))
				.andExpect(jsonPath("$.rating_text", is(dto.getRating_text()))).andExpect(jsonPath("$.user_id_for", is(dto.getUser_id_for())))
				.andExpect(jsonPath("$.user_id_from", is(dto.getUser_id_from()))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		RatingDTO dto = RatingDTO.createRandom();
		mockMvc.perform(put("/ratings/{id}", 0).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().is4xxClientError())
				.andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/ratings/{id}", 0).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError()).andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntity_thenStatus200_widgetsReturnedHasSize1() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/ratings/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent()).andReturn();
		mockMvc.perform(get("/ratings").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(testEntities[1].getId()))).andReturn();
	}

	private void createTestEntities(int count) throws EntityAlreadyExistsException {
		testEntities = new RatingEntity[count];
		for (int i = 0; i < count; i++) {
			testEntities[i] = RatingEntity.createRandom();
			repository.save(testEntities[i]);
		}
	}
}