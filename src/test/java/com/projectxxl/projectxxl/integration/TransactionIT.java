package com.projectxxl.projectxxl.integration;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectxxl.projectxxl.controllers.dto.TransactionDTO;
import com.projectxxl.projectxxl.domain.exception.EntityAlreadyExistsException;
import com.projectxxl.projectxxl.domain.model.TransactionEntity;
import com.projectxxl.projectxxl.domain.repository.TransactionRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TransactionIT {
	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private TransactionRepository repository;
	@Autowired
	private ObjectMapper objectMapper;
	private TransactionEntity[] testEntities;

	@BeforeEach
	public void init() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void givenEntities_whenGetEntity_thenStatus200() throws Exception {
		createTestEntities(5);
		mockMvc.perform(get("/transactions/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(testEntities[0].getId()))).andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
				.andExpect(jsonPath("$.modifiedAt", is(testEntities[0].getModifiedAt()), Long.class)).andExpect(jsonPath("$.amount", is(testEntities[0].getAmount()), Double.class))
				.andExpect(jsonPath("$.answer_id", is(testEntities[0].getAnswer_id()))).andExpect(jsonPath("$.question_id", is(testEntities[0].getQuestion_id())))
				.andExpect(jsonPath("$.status", is(testEntities[0].getStatus()))).andExpect(jsonPath("$.type", is(testEntities[0].getType()))).andReturn();
	}

	@Test
	public void givenNoEntities_whenCreateEntities_thenStatus201_widgetsReturnedHasSize1() throws Exception {
		TransactionDTO dto = TransactionDTO.createRandom();
		mockMvc.perform(post("/transactions").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(any(String.class)))).andExpect(jsonPath("$.createdAt", is(any(Long.class)), Long.class))
				.andExpect(jsonPath("$.modifiedAt", is(any(Long.class)), Long.class)).andExpect(jsonPath("$.amount", is(dto.getAmount()), Double.class))
				.andExpect(jsonPath("$.answer_id", is(dto.getAnswer_id()))).andExpect(jsonPath("$.question_id", is(dto.getQuestion_id())))
				.andExpect(jsonPath("$.status", is(dto.getStatus()))).andExpect(jsonPath("$.type", is(dto.getType()))).andReturn();
		mockMvc.perform(get("/transactions").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntity_thenStatus200() throws Exception {
		createTestEntities(2);
		TransactionDTO dto = TransactionDTO.createRandom();
		mockMvc.perform(put("/transactions/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.id", is(testEntities[0].getId())))
				.andExpect(jsonPath("$.createdAt", is(testEntities[0].getCreatedAt()), Long.class))
				.andExpect(jsonPath("$.modifiedAt", not(testEntities[0].getModifiedAt()), Long.class)).andExpect(jsonPath("$.amount", is(dto.getAmount()), Double.class))
				.andExpect(jsonPath("$.answer_id", is(dto.getAnswer_id()))).andExpect(jsonPath("$.question_id", is(dto.getQuestion_id())))
				.andExpect(jsonPath("$.status", is(dto.getStatus()))).andExpect(jsonPath("$.type", is(dto.getType()))).andReturn();
	}

	@Test
	public void givenEntities_whenUpdateEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		TransactionDTO dto = TransactionDTO.createRandom();
		mockMvc.perform(put("/transactions/{id}", 0).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().is4xxClientError())
				.andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntityWithNonExistentId_thenStatus400() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/transactions/{id}", 0).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError()).andReturn();
	}

	@Test
	public void givenEntities_whenDeleteEntity_thenStatus200_widgetsReturnedHasSize1() throws Exception {
		createTestEntities(2);
		mockMvc.perform(delete("/transactions/{id}", testEntities[0].getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent()).andReturn();
		mockMvc.perform(get("/transactions").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
				.andExpect(jsonPath("$[0].id", is(testEntities[1].getId()))).andReturn();
	}

	private void createTestEntities(int count) throws EntityAlreadyExistsException {
		testEntities = new TransactionEntity[count];
		for (int i = 0; i < count; i++) {
			testEntities[i] = TransactionEntity.createRandom();
			repository.save(testEntities[i]);
		}
	}
}