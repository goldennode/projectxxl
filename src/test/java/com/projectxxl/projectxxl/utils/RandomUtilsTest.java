package com.projectxxl.projectxxl.utils;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

public class RandomUtilsTest {
	@Test
	public void randomDouble_int_max() {
		Map<Integer, Integer> map = new HashMap<>();
		map.put(0, 0);
		map.put(1, 0);
		map.put(2, 0);
		map.put(3, 0);
		map.put(4, 0);
		map.put(5, 0);
		map.put(6, 0);
		for (int i = 0; i < 10000000; i++) {
			int rnd = (int) RandomUtils.randomDouble(6);
			Assert.isTrue(map.keySet().contains(rnd), "Incorrect results");
			map.put(rnd, 1);
		}
		Assert.isTrue(!map.values().contains(0), "Incorrect results");
	}

	@Test
	public void randomDouble_int_min_int_max() {
		Map<Integer, Integer> map = new HashMap<>();
		map.put(1, 0);
		map.put(2, 0);
		map.put(3, 0);
		map.put(4, 0);
		map.put(5, 0);
		map.put(6, 0);
		for (int i = 0; i < 10000000; i++) {
			int rnd = (int) RandomUtils.randomDouble(1, 6);
			Assert.isTrue(map.keySet().contains(rnd), "Incorrect results");
			map.put(rnd, 1);
		}
		Assert.isTrue(!map.values().contains(0), "Incorrect results");
	}

	@Test
	public void randomInt_int_max() {
		Map<Integer, Integer> map = new HashMap<>();
		map.put(0, 0);
		map.put(1, 0);
		map.put(2, 0);
		map.put(3, 0);
		map.put(4, 0);
		map.put(5, 0);
		map.put(6, 0);
		for (int i = 0; i < 10000000; i++) {
			int rnd = RandomUtils.randomInt(6);
			Assert.isTrue(map.keySet().contains(rnd), "Incorrect results");
			map.put(rnd, 1);
		}
		Assert.isTrue(!map.values().contains(0), "Incorrect results");
	}

	@Test
	public void randomInt_int_min_int_max() {
		Map<Integer, Integer> map = new HashMap<>();
		map.put(1, 0);
		map.put(2, 0);
		map.put(3, 0);
		map.put(4, 0);
		map.put(5, 0);
		map.put(6, 0);
		for (int i = 0; i < 10000000; i++) {
			int rnd = RandomUtils.randomInt(1, 6);
			Assert.isTrue(map.keySet().contains(rnd), "Incorrect results");
			map.put(rnd, 1);
		}
		Assert.isTrue(!map.values().contains(0), "Incorrect results");
	}
}
