package com.projectxxl.projectxxl.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@WebMvcTest(VersionController.class)
@ActiveProfiles("H2")
public class VersionControllerTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getVersion() throws Exception {
		MvcResult res = mockMvc.perform(get("/version").contentType(MediaType.TEXT_PLAIN)).andExpect(status().isOk()).andReturn();
		System.out.println(res.getResponse().getContentAsString());
	}
}