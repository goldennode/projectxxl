package com.projectxxl.projectxxl.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.projectxxl.projectxxl.controllers.dto.AnswerDTO;
import com.projectxxl.projectxxl.converters.AnswerToDomain;
import com.projectxxl.projectxxl.domain.PageData;
import com.projectxxl.projectxxl.domain.model.AnswerEntity;
import com.projectxxl.projectxxl.service.AnswerService;
import com.projectxxl.projectxxl.utils.RandomUtils;

//TODO add missing tests(should have same as integration counterparts) 
@ExtendWith(SpringExtension.class)
@WebMvcTest(AnswerController.class)
public class AnswerControllerTest {
	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private AnswerService entityService;
	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void createEntity() throws Exception {
		String id = RandomUtils.randomId();
		AnswerDTO dto = AnswerDTO.createRandom();
		AnswerEntity entity = new AnswerToDomain().convert(dto);
		AnswerEntity entityReturned = entity.toBuilder().withId(id).build();
		when(entityService.createEntity(entity)).thenReturn(entityReturned);
		mockMvc.perform(post("/answers").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().isCreated())
				.andExpect(jsonPath("$.id", is(id))).andReturn();
	}

	@Test
	public void updateEntity() throws Exception {
		String id = RandomUtils.randomId();
		AnswerDTO dto = AnswerDTO.createRandom();
		AnswerEntity entity = new AnswerToDomain().convert(dto.toBuilder().withId(id).build());
		AnswerEntity entityInPersistence = new AnswerToDomain().convert(dto.toBuilder().withId(id).withModifiedAt(System.currentTimeMillis() - 86400000 * 2).build());
		AnswerEntity entityReturned = new AnswerToDomain().convert(dto.toBuilder().withId(id).withModifiedAt(System.currentTimeMillis()).build());
		when(entityService.updateEntity(entity)).thenReturn(entityReturned);
		when(entityService.getEntity(id)).thenReturn(entityInPersistence);
		mockMvc.perform(put("/answers/{id}", id).contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(dto))).andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(id))).andReturn();
	}

	@Test
	public void deleteEntity() throws Exception {
		String id = RandomUtils.randomId();
		mockMvc.perform(delete("/answers/{id}", id).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNoContent()).andReturn();
	}

	@Test
	public void getEntity() throws Exception {
		String id = RandomUtils.randomId();
		AnswerEntity userEntity = AnswerEntity.createRandom().toBuilder().withId(id).build();
		when(entityService.getEntity(id)).thenReturn(userEntity);
		mockMvc.perform(get("/answers/{id}", id).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$.id", is(id))).andReturn();
	}

	@Test
	public void getAllEntities() throws Exception {
		String id1 = RandomUtils.randomId();
		String id2 = RandomUtils.randomId();
		AnswerEntity[] entities = new AnswerEntity[2];
		entities[0] = AnswerEntity.createRandom().toBuilder().withId(id1).build();
		entities[1] = AnswerEntity.createRandom().toBuilder().withId(id2).build();
		when(entityService.getAllEntities(0, 10)).thenReturn(new PageData<AnswerEntity>(entities, 0, 1, 2));
		mockMvc.perform(get("/answers").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$[0].id", is(id1)))
				.andExpect(jsonPath("$[1].id", is(id2))).andReturn();
	}

	@Test
	public void getAllEntities_pagination_1() throws Exception {
		String id1 = RandomUtils.randomId();
		AnswerEntity[] entities = new AnswerEntity[1];
		entities[0] = AnswerEntity.createRandom().toBuilder().withId(id1).build();
		when(entityService.getAllEntities(1, 1)).thenReturn(new PageData<AnswerEntity>(entities, 0, 1, 1));
		mockMvc.perform(get("/answers?page=1&pageSize=1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)));
	}

	@Test
	public void getAllEntities_pagination_2() throws Exception {
		String id1 = RandomUtils.randomId();
		String id2 = RandomUtils.randomId();
		AnswerEntity[] entities = new AnswerEntity[2];
		entities[0] = AnswerEntity.createRandom().toBuilder().withId(id1).build();
		entities[1] = AnswerEntity.createRandom().toBuilder().withId(id2).build();
		when(entityService.getAllEntities(0, 2)).thenReturn(new PageData<AnswerEntity>(entities, 0, 1, 2));
		mockMvc.perform(get("/answers?page=0&pageSize=2").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(2)));
	}
}