AWSTemplateFormatVersion: 2010-09-09
Description: Stack ECS ELB
Parameters:
  publicSubnet1:
    Description: publicSubnet1
    Type: String
  publicSubnet2:
    Description: publicSubnet2
    Type: String
  privateSubnet1:
    Description: privateSubnet1
    Type: String
  privateSubnet2:
    Description: privateSubnet2
    Type: String
  securityGroupPublic:
    Description: securityGroupPublic
    Type: String
  securityGroupPrivate:
    Description: securityGroupPrivate
    Type: String
  vpcId:
    Description: vpcId
    Type: String
  dockerImage:
    Description: dockerImage
    Type: String
  serviceName:
    Description: serviceName
    Type: String
  clusterName:
    Description: clusterName
    Type: String
  containerPort:
    Description: containerPort
    Type: Number
  healthCheckPath:
    Description: healthCheckPath
    Type: String
  containerEnvVar1:
    Description: containerEnvVar1
    Type: String
Resources:
  cluster:
    Type: AWS::ECS::Cluster
    Properties:
      ClusterName: !Ref clusterName
  taskDefinition:
    Type: AWS::ECS::TaskDefinition
    # Makes sure the log group is created before it is used.
    DependsOn: logGroup
    Properties:
      # Name of the task definition. Subsequent versions of the task definition are grouped together under this name.
      Family: !Join ['', [!Ref serviceName, TaskDefinition]]
      # awsvpc is required for Fargate
      NetworkMode: awsvpc
      RequiresCompatibilities:
        - FARGATE
      # 256 (.25 vCPU) - Available memory values: 0.5GB, 1GB, 2GB
      # 512 (.5 vCPU) - Available memory values: 1GB, 2GB, 3GB, 4GB
      # 1024 (1 vCPU) - Available memory values: 2GB, 3GB, 4GB, 5GB, 6GB, 7GB, 8GB
      # 2048 (2 vCPU) - Available memory values: Between 4GB and 16GB in 1GB increments
      # 4096 (4 vCPU) - Available memory values: Between 8GB and 30GB in 1GB increments
      Cpu: 256
      # 0.5GB, 1GB, 2GB - Available cpu values: 256 (.25 vCPU)
      # 1GB, 2GB, 3GB, 4GB - Available cpu values: 512 (.5 vCPU)
      # 2GB, 3GB, 4GB, 5GB, 6GB, 7GB, 8GB - Available cpu values: 1024 (1 vCPU)
      # Between 4GB and 16GB in 1GB increments - Available cpu values: 2048 (2 vCPU)
      # Between 8GB and 30GB in 1GB increments - Available cpu values: 4096 (4 vCPU)
      Memory: 2GB
      # A role needed by ECS.
      # "The ARN of the task execution role that containers in this task can assume. All containers in this task are granted the permissions that are specified in this role."
      # "There is an optional task execution IAM role that you can specify with Fargate to allow your Fargate tasks to make API calls to Amazon ECR."
      ExecutionRoleArn: !Ref executionRole
      # "The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that grants containers in the task permission to call AWS APIs on your behalf."
      TaskRoleArn: !Ref taskRole
      ContainerDefinitions:
        - Name: !Join ['', [!Ref serviceName, Container]]
          Image: !Ref dockerImage
          PortMappings:
            - ContainerPort: !Ref containerPort
          Environment:
            - Name: spring.profiles.active
              Value: !Ref containerEnvVar1
          # Send logs to CloudWatch Logs
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-region: !Ref AWS::Region
              awslogs-group: !Ref logGroup
              awslogs-stream-prefix: ecs
  # A role needed by ECS
  executionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', [!Ref serviceName, ExecutionRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy'
  # A role for the containers
  taskRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', [!Ref serviceName, TaskRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      # ManagedPolicyArns:
      #   -
      # Policies:
      #   -
  # A role needed for auto scaling
  autoScalingRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Join ['', [!Ref serviceName, AutoScalingRole]]
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: 'sts:AssumeRole'
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole'
  service:
    Type: AWS::ECS::Service
    # This dependency is needed so that the load balancer is setup correctly in time
    DependsOn:
      - listenerHTTP
    Properties: 
      ServiceName: !Ref serviceName
      Cluster: !Ref cluster
      TaskDefinition: !Ref taskDefinition
      DeploymentConfiguration:
        MinimumHealthyPercent: 50
        MaximumPercent: 200
      DesiredCount: 2
      # This may need to be adjusted if the container takes a while to start up
      HealthCheckGracePeriodSeconds: 30
      LaunchType: FARGATE
      NetworkConfiguration: 
        AwsvpcConfiguration:
          # change to DISABLED if you're using private subnets that have access to a NAT gateway
          AssignPublicIp: DISABLED
          Subnets:
            - !Ref privateSubnet1
            - !Ref privateSubnet2
          SecurityGroups:
            - !Ref securityGroupPrivate
      LoadBalancers:
        - ContainerName: !Join ['', [!Ref serviceName, Container]]
          ContainerPort: !Ref containerPort
          TargetGroupArn: !Ref targetGroup
  targetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckIntervalSeconds: 30
      Matcher: 
         HttpCode: 200-299
      HealthCheckPath: !Ref healthCheckPath
      HealthCheckTimeoutSeconds: 10
      UnhealthyThresholdCount: 3
      HealthyThresholdCount: 4
      Name: !Join ['', [!Ref serviceName, TargetGroup]]
      Port: !Ref containerPort
      Protocol: HTTP
      TargetGroupAttributes:
        - Key: deregistration_delay.timeout_seconds
          Value: 20 # default is 300
      TargetType: ip
      VpcId: !Ref vpcId
  listenerHTTP:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
        - TargetGroupArn: !Ref targetGroup
          Type: forward
      LoadBalancerArn: !Ref loadBalancer
      Port: 80
      Protocol: HTTP
  loadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      LoadBalancerAttributes:
        # this is the default, but is specified here in case it needs to be changed
        - Key: idle_timeout.timeout_seconds
          Value: 60
      Name: !Join ['', [!Ref serviceName, LoadBalancer]]
      # "internal" is also an option
      Scheme: internet-facing
      SecurityGroups:
        - !Ref securityGroupPublic
      Subnets:
        - !Ref publicSubnet1
        - !Ref publicSubnet2
  logGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Join ['', [/ecs/, !Ref serviceName, TaskDefinition]]
  autoScalingTarget:
    Type: AWS::ApplicationAutoScaling::ScalableTarget
    Properties:
      MinCapacity: 2
      MaxCapacity: 4
      ResourceId: !Join ['/', [service, !Ref cluster, !GetAtt service.Name]]
      ScalableDimension: ecs:service:DesiredCount
      ServiceNamespace: ecs
      # "The Amazon Resource Name (ARN) of an AWS Identity and Access Management (IAM) role that allows Application Auto Scaling to modify your scalable target."
      RoleARN: !GetAtt autoScalingRole.Arn
  autoScalingPolicy:
    Type: AWS::ApplicationAutoScaling::ScalingPolicy
    Properties:
      PolicyName: !Join ['', [!Ref serviceName, AutoScalingPolicy]]
      PolicyType: TargetTrackingScaling
      ScalingTargetId: !Ref autoScalingTarget
      TargetTrackingScalingPolicyConfiguration:
        PredefinedMetricSpecification:
          PredefinedMetricType: ECSServiceAverageCPUUtilization
        ScaleInCooldown: 10
        ScaleOutCooldown: 10
        # Keep things at or lower than 50% CPU utilization, for example
        TargetValue: 50
Outputs:
  dnsName:
    Description: dnsName
    Value: !GetAtt loadBalancer.DNSName
  canonicalHostedZoneID:
    Description: ELB DNS
    Value: !GetAtt loadBalancer.CanonicalHostedZoneID
    
    
    
    